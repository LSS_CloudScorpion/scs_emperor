#include "Core.hpp"
#include <EmperorEngine.hpp>
//jp hughes
using namespace Emperor;
using namespace Graphics;

Core::Core() : gfxEngine(0) {}


Core::~Core()
   {
   if(gfxEngine)
      releaseEngine(gfxEngine);
   gfxEngine = 0;
   }

void Core::init()
   {
   //Emperor::setLogger(fileLogger);

   if(!settings.loadFromFile("settings.ini"))
      LOG("Settings file failed to load. Running on defaults...");

   gfxEngine = Graphics::createEngine(settings.renderSystem, settings.resourceFolder);

   //We need a window to draw on
   w1 = Utility::getWindowController()->createWindow();
   w1->setName("testing window");
   w1->setSize(settings.resolution);
   w1->init();
   Input::getController()->bindWindow(w1);

   //Activate the engine on the window
   gfxEngine->setMultisampleCount(settings.multisampleCount);
   gfxEngine->activateDevice(w1);
   gfxEngine->setFullScreen(settings.fullscreen);
   gfxEngine->setVsync(settings.vsync);
   gfxEngine->initialize();

   }

uint64 t = 0;
void Core::run()
   {
   while(1)
      {
      auto msg = Utility::getWindowController()->retrieveWindowMessages().stdVector();
      gfxEngine->render();

      }
   if(!settings.saveToFile("settings.ini"))
      LOG("Settings file failed to save. Changes have been lost");
   }

void Core::halt()
   {

   }