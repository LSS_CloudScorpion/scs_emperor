#include "Core.hpp"
#include <exception>
#include <iostream>

int main(int argc, const char* argv[])
   {
   try
      {
      Core c;
      c.init();
      c.run(); 
      }
   catch(std::exception& e)
      {
      std::cout << e.what();
      int i;
      std::cin >> i;
      }
   return 0;
   }