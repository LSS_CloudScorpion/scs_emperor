#include "InternalTypes.hpp"
#include "WindowManager.hpp"


namespace Emperor
   {
   namespace Utility
      {
      ArrayList<MessagePackage> WindowManager::retrieveWindowMessages()
         {
         messageList.clear();
         for(auto i = objects.begin(); i < objects.end(); ++i)
            {
            currentProcess = (*i);
            (*i)->_processMessage();
            }
         currentProcess = 0;
         return messageList;
         }

      void WindowManager::collectMessage(WindowMessage message)
         {
         messageList.push_back(MessagePackage(currentProcess, message));
         }
      }
   }