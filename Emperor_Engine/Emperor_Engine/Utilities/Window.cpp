#if EMP_USE_WINDOWS

#include "../Windows.hpp"
#include "Window.hpp"
#include "WindowManager.hpp"
#include "../Graphics/Managers.hpp"

namespace Emperor
   {
   namespace Utility
      {
      void Window::init()
         {
         //Retrieve the application handle and give the window class a default
         //name if it does not already have a name set, then set the class
         //Adjust the window dimensions to match the size attribute with a 
         //position of 0,0, use a RECT struct to accomplish this
         //Create the window (casting the windowStyle into a DWORD*) and store
         //the handle in the window handle attribute
         //Retreive the window placement and reveal the window
         }

      void Window::showWindow(bool a)
         {
         //Fill Me
         //Note: use null to hide window
         }

      void Window::destroy()
         {
         //Destroy the window, remove from manager, deallocate
         }

      void Window::setName(const LString& name)
         {
         //Fill Me
         }

      void Window::setClass(const WindowClass& cl)
         {
         windowClass = cl;
         WNDCLASS w;
         w.style = 0;
         w.cbClsExtra = 0;
         w.cbWndExtra = 0;
         w.hIcon = 0;
         w.hCursor = 0;
         w.hbrBackground = 0;
         w.lpszMenuName = 0;
         w.lpfnWndProc = _windowProcess;
         w.hInstance = appInst;
         w.lpszClassName = &cl.className.front();
         RegisterClass(&w);
         }

      void Window::setPosition(const Vector<int, 2>& pos)
         {
         //Fill Me
         }

      void Window::setSize(const Vector<int, 2>& sz)
         {
         //Fill Me
         }

      void Window::_processMessage()
         {
         //Loop while the message queue is not empty
         //Retreive the next message, translate it, and dispatch it
         }

      LRESULT CALLBACK Window::_windowProcess(HWND hWnd, UINT msg, WPARAM wParam,
         LPARAM lParam)
         {
         //Translate the message using _translateMessage, the pass the message
         //to the Window Manager's collect message function
         return 0;
         }
      }
   }

#endif