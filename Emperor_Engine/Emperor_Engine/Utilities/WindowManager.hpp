#ifndef __EMP_UTL_WINDOW_MANAGER_HPP__
#define __EMP_UTL_WINDOW_MANAGER_HPP__

#include "../BaseManager.hpp"
#include "Window.hpp"

namespace Emperor
   {
   namespace Utility
      {
      class WindowManager : public Singleton<WindowManager>, 
         public BaseManager<Window>
         {
         private:
            ArrayList<MessagePackage> messageList;
            Window* currentProcess;
         protected:
         public:
            WindowManager() : currentProcess(0) {}
            virtual ~WindowManager() {}

            ArrayList<MessagePackage> retrieveWindowMessages();
            void collectMessage(WindowMessage message);
         };
      }
   }

#endif