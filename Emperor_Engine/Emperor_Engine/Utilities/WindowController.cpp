#include "WindowController.hpp"

namespace Emperor
   {
   namespace Utility
      {
      iWindow* WindowController::createWindow() 
         { return winMan.createObject(); }
      LArray<MessagePackage> WindowController::retrieveWindowMessages()
         { return winMan.retrieveWindowMessages(); }

      EMP_API iWindowController* getWindowController()
         {
         if(!WindowController::getPtr())
            new WindowController();
         return WindowController::getPtr();
         }
      }
   }