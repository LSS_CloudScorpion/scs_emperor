#ifndef __EMP_WINDOWS_HPP__
#define __EMP_WINDOWS_HPP__

#if EMP_USE_WINDOWS

//undefine potentially hiding danger...
#ifdef _WIN32_WINNT
#undef _WIN32_WINNT
#endif
#define _WIN32_WINNT 0x601
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#endif

#endif