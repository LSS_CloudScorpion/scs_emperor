#ifndef __EMP_BASE_MANAGER_HPP__
#define __EMP_BASE_MANAGER_HPP__

#include "Internals.hpp"

namespace Emperor
   {
   template <class T>
   class BaseManager
      {
      private:
      protected:
         ArrayList<T*> objects;
         ArrayList<T*> activeObjects;
      public:
         BaseManager() {}
         virtual ~BaseManager()
            {
            //Iterator over all objects and destroy them
            //Keep in mind that the destroy function automatically removes
            //them from the manager's array
            }

         T* createObject()
            {
            //Fill Me
            return new T;
            }

         void removeObject(T* o)
            {
            //Search the list to find the object to remove, then remove it
            //Do not destroy the object
            }

         void activateObject(T* o)
            {
            //Fill Me
            }

         void deactivateObject(T* o)
            {
            //Fill Me
            }
      };

   template <class T>
   class IterativeManager : public BaseManager<T>
      {
      private:
      protected:
         typename ArrayList<T*>::iterator it;

      public:
         IterativeManager() {}
         virtual ~IterativeManager() {}

         void prepareLoop() {it = activeObjects.begin();}

         T* getCurrent() {return *(it - 1);}
      };
   }

#endif