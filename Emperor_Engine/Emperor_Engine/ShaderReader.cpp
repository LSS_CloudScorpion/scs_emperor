#include "ShaderReader.hpp"

namespace Emperor
   {
   void decrypeShader(ArrayList<char>& a)
      {
      std::string b("What a terrible encryption!");

      auto j = b.begin();
      for(auto i = a.begin(),end = a.end(); i != end; i++)
         {
         (*i) ^= (*j);
         if(++j == b.end())
            j = b.begin();
         }
      }
   }