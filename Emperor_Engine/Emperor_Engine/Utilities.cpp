#define EMP_EXPORT
#include <Core/EmperorAPI.hpp>
#include <iostream>
#include "Utilities.hpp"


namespace Emperor
   {

   static void _logDefault(const char* c)
      {
      std::cout << c << std::endl;
      }

   void(*_logFunc)(const char*) = _logDefault;

   void _logCall()
      {
      _Logger::string = _Logger::stream.str();
      _logFunc(_Logger::string.c_str());
      _Logger::stream.str(std::string());
      _Logger::stream.clear();
      }

   EMP_API void setLogger(void(*func)(const char*))
      {
      if(func)
         _logFunc = func;
      else
         _logFunc = _logDefault;
      }


   std::stringstream _Logger::stream;
   std::string _Logger::string;


   }