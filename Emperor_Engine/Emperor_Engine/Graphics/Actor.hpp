#ifndef __EMP_GFX_ACTOR_HPP__
#define __EMP_GFX_ACTOR_HPP__


#include <Graphics/Types.hpp>
#include "SceneObject.hpp"
#include <Graphics/iActor.hpp>

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS> class Material;
      template <RenderSystem RS> class Mesh;

      template <RenderSystem RS>
      class Actor : public SceneObject<RS, iActor>
         {
         private:
            Material<RS>* mat;
            Mesh<RS>* mesh;
         protected:
            virtual ~Actor() {}
            void update();
         public:
            Actor();

            void destroy();

            void setMesh(const iMesh*);
            void setMesh(const LString&);
            iMesh* getMesh() { return (iMesh*)mesh; }

            void setMaterial(const iMaterial*);
            void setMaterial(const LString&);
            iMaterial* getMaterial() { return (iMaterial*)mat; }

            void activate();
            void deactivate();
         };
      }
   }

#endif