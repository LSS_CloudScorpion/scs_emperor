#include "APIBuffer.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_DIRECTX
      APIBuffer<RS_DX11>::~APIBuffer()
         {
         Release();
         }

      void APIBuffer<RS_DX11>::Release()
         {
         if(buffer)
            {
            buffer->Release();
            buffer = 0;
            }
         }

      void APIVertexBuffer<RS_DX11>::finalize(const iVertexFormat&, uint32 size)
         {
         //Fill Me
         }

      void APIIndexBuffer<RS_DX11>::finalize(uint32 size)
         {
         //Fill Me
         }

      void APIUniformBuffer<RS_DX11>::finalize(uint32 size)
         {
         //Fill Me

         //size needs to be a multiple of 16, if it is not, the size must be
         //set to the next largest multiple of 16
         //Example: size of 28 would need to be changed to 32
         }

      void APIBuffer<RS_DX11>::fill(const byte* b, uint32 size)
         {
         //Fill Me
         }

      void APIBuffer<RS_DX11>::lock()
         {
         //maps the buffer to attribute sub, use EMP_CONTEXT_ASSERT_DX
         }

      void APIBuffer<RS_DX11>::unlock()
         {
         //Fill Me
         }

      template <class T>
      void APIBuffer<RS_DX11>::setData(uint32 i, T d)
         {
         //Fill Me
         }

      void APIBuffer<RS_DX11>::setData(uint32 i, void* d, uint32 s)
         {
         //Fill Me
         //Note: will need to cast to byte* and offset the pointer
         }

      void APIVertexBuffer<RS_DX11>::bind(uint32 vertexSize) const
         {
         //Fill Me

         //Note: Use singletons to expose context
         }

      void APIIndexBuffer<RS_DX11>::bind() const
         {
         //Fill Me

         //Note: Use singletons to expose context
         }

      void APIUniformBuffer<RS_DX11>::bindToVertex(uint32 slot) const
         {
         //Fill Me

         //Note: Use singletons to expose context
         }

      void APIUniformBuffer<RS_DX11>::bindToGeometry(uint32 slot) const
         {
         //Fill Me

         //Note: Use singletons to expose context
         }

      void APIUniformBuffer<RS_DX11>::bindToFragment(uint32 slot) const
         {
         //Fill Me

         //Note: Use singletons to expose context
         }
#endif

#if EMP_USE_OPENGL
      APIBuffer<RS_GL43>::~APIBuffer()
         {
         Release();
         }

      void APIBuffer<RS_GL43>::Release()
         {

         }

      static void _initVertex(const VertexFormat<RS_GL43>& v)
         {
         uint32 i = 0;
         auto a = v.getVertexDescription().stdVector();
         for(auto j = a.begin(), end = a.end(); j != end; j++, i++)
            {
            uint32 size;
            auto type = _glExtractFormat(j->format, size);
            glVertexAttribPointer(i, size, type, GL_FALSE, v.getByteSize(), (GLvoid*)j->aligned);
            }
         }

      void APIVertexBuffer<RS_GL43>::finalize(const iVertexFormat& v, uint32 size)
         {
         //Fill for GL Component
         //Note: you must cast from iVertexFormat to VertexFormat
         //Throw exception with EMP_DEVICE_ERROR
         }

      //get rid of useless param
      void APIIndexBuffer<RS_GL43>::finalize(uint32 size)
         {
         //Fill for GL Component
         }

      void APIUniformBuffer<RS_GL43>::finalize(uint32 size)
         {
         //Fill for GL Component
         }

      void APIVertexBuffer<RS_GL43>::fill(const byte* d, uint32 i)
         {
         //Fill for GL Component
         }

      void APIIndexBuffer<RS_GL43>::fill(const byte* d, uint32 i)
         {
         //Fill for GL Component
         }

      void APIUniformBuffer<RS_GL43>::fill(const byte* d, uint32 i)
         {
         //Fill for GL Component
         }

      void APIVertexBuffer<RS_GL43>::lock()
         {
         //Fill for GL Component
         }

      void APIIndexBuffer<RS_GL43>::lock()
         {
         //Fill for GL Component
         }

      void APIUniformBuffer<RS_GL43>::lock()
         {
         //Fill for GL Component
         }

      void APIVertexBuffer<RS_GL43>::unlock()
         {
         //Fill for GL Component
         }

      void APIIndexBuffer<RS_GL43>::unlock()
         {
         //Fill for GL Component
         }

      void APIUniformBuffer<RS_GL43>::unlock()
         {
         //Fill for GL Component
         }

      template <class T>
      void APIBuffer<RS_GL43>::setData(uint32 i, T d)
         {
         //Fill for GL Component
         }

      void APIBuffer<RS_GL43>::setData(uint32 i, void* d, uint32 s)
         {
         //Fill for GL Component
         }

      void APIVertexBuffer<RS_GL43>::bind(uint32) const
         {
         //Fill for GL Component
         }

      void APIIndexBuffer<RS_GL43>::bind() const
         {
         //Fill for GL Component
         }

      void APIUniformBuffer<RS_GL43>::bindToVertex(uint32 index) const
         {
         //Fill for GL Component
         }

      void APIUniformBuffer<RS_GL43>::bindToGeometry(uint32 index) const
         {
         //Fill for GL Component
         }

      void APIUniformBuffer<RS_GL43>::bindToFragment(uint32 index) const
         {
         //Fill for GL Component
         }
#endif
      }
   }