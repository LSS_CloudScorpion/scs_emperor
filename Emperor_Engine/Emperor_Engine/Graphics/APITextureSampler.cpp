#include "APITextureSampler.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_DIRECTX
      APITextureSampler<RS_DX11>::~APITextureSampler()
         {
         if(sampler)
            sampler->Release();
         }

      void APITextureSampler<RS_DX11>::bindToVertex(uint32 slot) const
         {
         //Fill Me
         //Use singleton to expose the context
         }

      void APITextureSampler<RS_DX11>::bindToGeometry(uint32 slot) const
         {
         //Fill Me
         //Use singleton to expose the context
         }

      void APITextureSampler<RS_DX11>::bindToFragment(uint32 slot) const
         {
         //Fill Me
         //Use singleton to expose the context
         }

      void APITextureSampler<RS_DX11>::build(const TextureFilter& tf, TextureAddressMode u,
         TextureAddressMode v, TextureAddressMode w,
         float mlb, uint32 ma, TextureComparisonMethod cm,
         const Color& bc, float nl, float xl)
         {
         //Use translation functions to set sampler description, see
         //APITranslations.hpp
         //Use EMP_DEVICE_ASSERT_DX to emit exception if creation fails
         }
#endif

#if EMP_USE_OPENGL
      APITextureSampler<RS_GL43>::~APITextureSampler()
         {

         }

      void APITextureSampler<RS_GL43>::build(const TextureFilter& tf, TextureAddressMode u,
         TextureAddressMode v, TextureAddressMode w, float mlb, uint32 ma, TextureComparisonMethod cm,
         const Color& bc, float nl, float xl)
         {
         //Use translation functions to set sampler parameters, see
         //APITranslations.hpp
         //If a gl error occurs, emit error with EMP_DEVICE_ERROR
         }

      void APITextureSampler<RS_GL43>::bindToVertex(uint32 slot) const
         {
         //Fill Me
         }

      void APITextureSampler<RS_GL43>::bindToGeometry(uint32 slot) const
         {
         //Fill Me
         }

      void APITextureSampler<RS_GL43>::bindToFragment(uint32 slot) const
         {
         //Fill Me
         }
#endif
      }
   }