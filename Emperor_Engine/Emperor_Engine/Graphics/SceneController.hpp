#ifndef __EMP_GFX_SCENE_CONTROLLER_HPP__
#define __EMP_GFX_SCENE_CONTROLLER_HPP__

#include <Graphics/iSceneController.hpp>
#include "Managers.hpp"
#include "../NodeManager.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class SceneController : public iSceneController
         {
         private:
         protected:
         public:
            ActorManager<RS> actMan;
            CameraManager<RS> camMan;
            LightManager<RS> lgtMan;
            NodeManager<RS> nodMan;

            //LAB: Should call the create object function on the appropriate
            //manager

            iActor* createActor()
               { return new Actor<RS>; }
            iCamera* createCamera()
               { return new Camera<RS>; }
            iLight* createLight()
               { return new Light<RS>; }

            //This one is correct, leave it alone
            iNode* createNode()
               { return Emperor::NodeManager::getPtr()->createObject(); }
         };
   }
   }

#endif