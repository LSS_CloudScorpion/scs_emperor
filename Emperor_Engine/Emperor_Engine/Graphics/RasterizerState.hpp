#ifndef __EMP_GFX_RASTERIZER_STATE_HPP__
#define __EMP_GFX_RASTERIZER_STATE_HPP__

#include <Graphics/Types.hpp>
#include "APIRasterizerState.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class RasterizerState
         {
         private:
            APIRasterizerState<RS> rast;
         protected:
         public:
            RasterizerState() {}
            ~RasterizerState() {}

            void initRasterState(const RasterizerStateDescription& r) { rast.init(r); }

            void bindRenderer() { rast.bind(); }
         };
      }
   }

#endif