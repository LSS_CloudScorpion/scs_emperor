#include "Device.hpp"
#include "APITranslations.hpp"
#include "Engine.hpp"
#include "../Utilities/WindowController.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_DIRECTX
      Device<RS_DX11>::~Device()
         {
         release();
         }

      void Device<RS_DX11>::init(Utility::Window* w)
         {
         //Use EMP_INIT_ASSERT_DX to throw exceptions when API calls fail
         //Create the device and swap chain using reasonable settings (check
         //course slides for examples)

         //Create and set the backbuffer and depth buffer

         //Create and set view port (based on window size)

         //set active to true
         }

      void Device<RS_DX11>::setVsync(uint32 v)
         {
         vsync = v;
         }

      void Device<RS_DX11>::renderStart()
         {
         //Fill Me
         }

      void Device<RS_DX11>::renderEnd()
         {
         //Fill Me
         }

      void Device<RS_DX11>::setFullScreen(bool b)
         {
         //Fill Me
         }

      void Device<RS_DX11>::release()
         {
         //Turns off fullscreen, releases swap chain, backbuffer, device, and
         //context
         }

      void Device<RS_DX11>::draw(uint32 size, uint32 offset)
         {
         //Fill Me
         }

      void Device<RS_DX11>::drawIndexed(uint32 size, uint32 iOffset, uint32 vOffset)
         {
         //Fill Me
         }

      void Device<RS_DX11>::bindTargets(RenderTarget<RS_DX11>* r, DepthStencil<RS_DX11>* d, bool exclusive, const Vector<uint32, 4>& vp)
         {
         //Fill Me
         }

      void Device<RS_DX11>::bindPrimitive(const PrimitiveTopology& p)
         {
         //Fill Me
         }
#endif

#if EMP_USE_OPENGL
      Device<RS_GL43>::~Device()
         {
         release();
         }

      void Device<RS_GL43>::init(Utility::Window* win)
         {
         //Fill for GL Component
         }

      void Device<RS_GL43>::release()
         {
         //Fill for GL Component
         }
      void Device<RS_GL43>::setVsync(uint32 v)
         {
         //Possible Enhancement
         }

      void Device<RS_GL43>::renderStart()
         {
         //Fill for GL Component
         }

      void Device<RS_GL43>::renderEnd()
         {
         //Fill for GL Component
         }


      void Device<RS_GL43>::setFullScreen(bool b)
         {
         //Fill for GL Component
         }

      void Device<RS_GL43>::bindPrimitive(const PrimitiveTopology& a)
         {
         //Fill for GL Component
         }

      void Device<RS_GL43>::draw(uint32 size, uint32 offset)
         {
         //Fill for GL Component
         }

      void Device<RS_GL43>::drawIndexed(uint32 size, uint32 iOffset, uint32 vOffset)
         {
         //Fill for GL Component
         }

      void Device<RS_GL43>::bindTargets(RenderTarget<RS_GL43>* r, DepthStencil<RS_GL43>* d, bool exclusive, const Vector<uint32, 4>& vp)
         {
         //Fill Me
         }


      void buildTangents(ArrayList<Vector<float, 4>>& tangents,
         ArrayList<Vector<float, 4>>& binormals,
         const ArrayList<Vector<float, 4>>& positions,
         const ArrayList<uint32>& indices,
         const ArrayList<Vector<float, 4>>& normals,
         const ArrayList<Vector<float, 4>>& texCoords)
         {
         for(uint32 i = 0; i < indices.size(); i += 3)
            {
            Vector<uint32, 3> tri;
            tri.x() = indices[i];
            tri.y() = indices[i + 1];
            tri.z() = indices[i + 2];
            int i1 = tri.x();
            int i2 = tri.y();
            int i3 = tri.z();

            auto& v1 = positions[i1];
            auto& v2 = positions[i2];
            auto& v3 = positions[i3];

            auto& w1 = texCoords[i1];
            auto& w2 = texCoords[i2];
            auto& w3 = texCoords[i3];

            float x1 = v2.x() - v1.x();
            float x2 = v3.x() - v1.x();
            float y1 = v2.y() - v1.y();
            float y2 = v3.y() - v1.y();
            float z1 = v2.z() - v1.z();
            float z2 = v3.z() - v1.z();

            float s1 = w2.x() - w1.x();
            float s2 = w3.x() - w1.x();
            float t1 = w2.y() - w1.y();
            float t2 = w3.y() - w1.y();

            float den = s1 * t2 - s2 * t1;
            float r = den != 0.0f ? 1.0f / den : 0;
            auto sdir = Vector3(
               (t2 * x1 - t1 * x2) * r,
               (t2 * y1 - t1 * y2) * r,
               (t2 * z1 - t1 * z2) * r
               );
            auto tdir = Vector3(
               (s1 * x2 - s2 * x1) * r,
               (s1 * y2 - s2 * y1) * r,
               (s1 * z2 - s2 * z1) * r
               );

            auto tan1 = Vector4(sdir, crossProduct(normals[i1].xyz(), sdir).dot(tdir) < 0 ? -1.0f : 1.0f);
            auto tan2 = Vector4(sdir, crossProduct(normals[i2].xyz(), sdir).dot(tdir) < 0 ? -1.0f : 1.0f);
            auto tan3 = Vector4(sdir, crossProduct(normals[i3].xyz(), sdir).dot(tdir) < 0 ? -1.0f : 1.0f);

            tangents[i1] = tan1;
            tangents[i2] = tan2;
            tangents[i3] = tan3;

            binormals[i1] = Vector4(crossProduct(tangents[i1].xyz(), normals[i1].xyz()), tan1.w());
            binormals[i2] = Vector4(crossProduct(tangents[i2].xyz(), normals[i1].xyz()), tan2.w());
            binormals[i3] = Vector4(crossProduct(tangents[i3].xyz(), normals[i1].xyz()), tan3.w());
            }
         }
#endif
      }
   }

