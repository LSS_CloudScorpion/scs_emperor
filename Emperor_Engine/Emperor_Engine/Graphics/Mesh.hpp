#ifndef __EMP_GFX_MESH_HPP__
#define __EMP_GFX_MESH_HPP__

#include <Graphics/iMesh.hpp>
#include <Graphics/Vertex.hpp>
#include "Buffers.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class Mesh : public iMesh
         {
         private:
            VertexBuffer<RS> vertex;
            IndexBuffer<RS> index;
            PrimitiveTopology prim;
         protected:

            virtual ~Mesh() {}
         public:
            Mesh() {}

            void loadFromList(const iVertexFormat& vertexDefinition,
               const LArray<const byte*>&,
               const LArray<uint32>&,
               const PrimitiveTopology&);
            void loadFromFile(const LString&);
            void loadFromMem(const LString&, const LString& = "[Unknown].obj");
            void _loadOBJ(const ArrayList<String>&);

            uint32 getVertexCount() const { return vertex.getIndexSize(); }
            uint32 getIndexCount() const { return index.getIndexSize(); }

            const PrimitiveTopology& getPrimitive() const { return prim; }

            void bindMesh() const;

            void destroy();
         };
      }
   }

#endif