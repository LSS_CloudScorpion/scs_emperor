#ifndef __EMP_GFX_INTERNAL_TYPES_HPP__
#define __EMP_GFX_INTERNAL_TYPES_HPP__

#include <Graphics/Types.hpp>
#include "../Internals.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      //Textures
      enum TextureType
         {
         TT_1D,
         TT_2D,
         TT_3D,
         TT_CUBE,
         TT_RENDER_TARGET,
         TT_DEPTH_STENCIL
         };
      }
   }

#endif