#include "APIBlendState.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_DIRECTX
      APIBlendState<RS_DX11>::~APIBlendState()
         {
         if(bs)
            bs->Release();
         }


      void APIBlendState<RS_DX11>::bindBlendState() const
         {
         //Fill Me
         //Note: use singleton to expose context
         }


      void APIBlendState<RS_DX11>::finalize(const HashMap<uint32, BlendStateDescription>& dList,
         bool independantBlend, bool alphaToCoverage)
         {
         //Iterate over (dList) and set the variables for each associated
         //render target.
         //Note: many of the variables require translation, check
         //APITranslations.hpp for a list of translation functions
         //Use EMP_DEVICE_ASSERT_DX when creating the blend state
         }
#endif

#if EMP_USE_OPENGL
      void APIBlendState<RS_GL43>::bindBlendState() const
         {
         //Iterate over (states), enabling each state (by index) and
         //setting the equation and function for each state
         }



      inline void _glEnProxi(GLuint i)
         {
         glEnablei(GL_BLEND, i);
         }

      inline void _glDsProxi(GLuint i)
         {
         glDisablei(GL_BLEND, i);
         }

      void APIBlendState<RS_GL43>::finalize(const HashMap<uint32, BlendStateDescription>& dList,
         bool independantBlend, bool alphaToCoverage)
         {
         //Iterate over (dList), creating a _glBlend object and setting each
         //attribute to the associated value in (dList).
         //Note: many of the variables require translation, check
         //APITranslations.hpp for a list of translation functions
         //Enable function should be set to either _glEnProxi or 
         //_glDsProxi
         }
#endif
      }
   }