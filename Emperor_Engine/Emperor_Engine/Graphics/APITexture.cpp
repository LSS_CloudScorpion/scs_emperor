#include "APITexture.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_DIRECTX
      APITexture<RS_DX11>::~APITexture()
         {
         if(texture)
            {
            texture->Release();
            texture = 0;
            }
         }

      void APITexture<RS_DX11>::loadFile(const String& s)
         {
         //Fill Me
         //Use singleton to expose the device
         
         }

      void APITexture<RS_DX11>::loadFromMem(byte* m, uint32 s)
         {
         HRESULT er;

         if(FAILED(er = D3DX11CreateShaderResourceViewFromMemory(
            Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeDevice(), m, s,
            0, 0, &texture, 0)))
            EMP_RESOURCE_ERROR(String("Unable to load texture [File Here]") + \
            ", using default\n   Error: " + std::to_string(er));
         }

      void APITexture<RS_DX11>::bindToVertex(uint32 slot) const
         {
         //Fill Me
         //Use singleton to expose the context
         }

      void APITexture<RS_DX11>::bindToGeometry(uint32 slot) const
         {
         //Fill Me
         //Use singleton to expose the context
         }

      void APITexture<RS_DX11>::bindToFragment(uint32 slot) const
         {
         //Fill Me
         //Use singleton to expose the context
         }

      APITexture1D<RS_DX11>::~APITexture1D()
         {
         if(tex1d)
            tex1d->Release();
         }

      void APITexture1D<RS_DX11>::init(uint32 size, StructureFormat format)
         {
         //Create a 1D Texture and a Shader Resource View, use
         //EMP_DEVICE_ASSERT_DX in both cases
         }

      void APITexture1D<RS_DX11>::fill(byte* data, uint32 size)
         {
         //Map the buffer and use EMP_CONTEXT_ASSERT_DX to emit exceptions
         //Once mapped, copy the values in (data) into the mapped memory
         //Unmap when done
         }

      APITexture2D<RS_DX11>::~APITexture2D()
         {
         if(tex2d)
            tex2d->Release();
         }

      void APITexture2D<RS_DX11>::init(uint32 width, uint32 height, StructureFormat format)
         {
         //Create a 2D Texture and a Shader Resource View, use
         //EMP_DEVICE_ASSERT_DX in both cases
         }


      void APITexture2D<RS_DX11>::fill(byte* data, uint32 size)
         {
         //Map the buffer and use EMP_CONTEXT_ASSERT_DX to emit exceptions
         //Once mapped, copy the values in (data) into the mapped memory
         //Unmap when done
         }

      APIRenderTarget<RS_DX11>::~APIRenderTarget()
         {
         //Fill Me
         }

      void APIRenderTarget<RS_DX11>::init(uint32 width, uint32 height, StructureFormat format)
         {
         //Fill Me
         }




      APIDepthStencil<RS_DX11>::~APIDepthStencil()
         {
         //Fill Me
         }

      void APIDepthStencil<RS_DX11>::init(uint32 width, uint32 height, StructureFormat format)
         {
         //Fill Me
         }


      APITexture3D<RS_DX11>::~APITexture3D()
         {
         if(tex3d)
            tex3d->Release();
         }

      void APITexture3D<RS_DX11>::init(uint32 width, uint32 height, uint32 depth, StructureFormat format)
         {
         //Create a 2D Texture and a Shader Resource View, use
         //EMP_DEVICE_ASSERT_DX in both cases
         }

      void APITexture3D<RS_DX11>::fill(byte* data, uint32 size)
         {
         //Map the buffer and use EMP_CONTEXT_ASSERT_DX to emit exceptions
         //Once mapped, copy the values in (data) into the mapped memory
         //Unmap when done
         }
#endif

#if EMP_USE_OPENGL
      APITexture<RS_GL43>::~APITexture()
         {
         glDeleteTextures(1, &textureID);
         }

      void APITexture<RS_GL43>::loadFile(const String& fn)
         {
         int w, h, c, s;
         unsigned char* data = SOIL_load_image(fn.c_str(), &w, &h, &c, 0);
         if(!data)
            EMP_RESOURCE_ERROR("File was unable to be loaded");
         glGenTextures(1, &textureID);
         dim = GL_TEXTURE_2D;
         glBindTexture(dim, textureID);
         switch(c)
            {
            case 1:
               format = GL_LUMINANCE;
               iFormat = GL_RGBA8;
               s = 3;
               break;
            case 2:
               format = GL_LUMINANCE_ALPHA;
               iFormat = GL_RGBA8;
               s = 4;
               break;
            case 3:
               format = GL_RGB;
               iFormat = GL_RGB8;
               s = 3;
               break;
            case 4:
               format = GL_RGBA;
               iFormat = GL_RGBA8;
               s = 4;
               break;
            }

         //flip the Y axis upside down
         int i, j;
         for(j = 0; j * 2 < h; ++j)
            {
            int index1 = j * w * s;
            int index2 = (h - 1 - j) * w * s;
            for(i = w * s; i > 0; --i)
               {
               unsigned char temp = data[index1];
               data[index1] = data[index2];
               data[index2] = temp;
               ++index1;
               ++index2;
               }
            }

         type = GL_UNSIGNED_BYTE;
         GLint maxTextureSize;
         glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
         glTexStorage2D(dim, intLog2(min(w, h)), iFormat, w, h);
         GLuint er = glGetError();
         glTexSubImage2D(dim, 0, 0, 0, w, h, format, type, data);
         glGenerateMipmap(dim);
         SOIL_free_image_data(data);
         er = glGetError();
         if(er != GL_NO_ERROR)
            EMP_DEVICE_ERROR(String("Failed to create texture!\nError code: "));
         }

      void APITexture<RS_GL43>::loadFromMem(byte* m, uint32 size)
         {
         int w, h, c, s;
         unsigned char* data = SOIL_load_image_from_memory(m, size, &w, &h, &c, 0);
         if(!data)
            EMP_RESOURCE_ERROR("File was unable to be loaded");
         glGenTextures(1, &textureID);
         dim = GL_TEXTURE_2D;
         glBindTexture(dim, textureID);
         switch(c)
            {
            case 1:
               format = GL_LUMINANCE;
               iFormat = GL_RGBA8;
               s = 3;
               break;
            case 2:
               format = GL_LUMINANCE_ALPHA;
               iFormat = GL_RGBA8;
               s = 4;
               break;
            case 3:
               format = GL_RGB;
               iFormat = GL_RGB8;
               s = 3;
               break;
            case 4:
               format = GL_RGBA;
               iFormat = GL_RGBA8;
               s = 4;
               break;
            }

         //flip the Y axis upside down
         int i, j;
         for(j = 0; j * 2 < h; ++j)
            {
            int index1 = j * w * s;
            int index2 = (h - 1 - j) * w * s;
            for(i = w * s; i > 0; --i)
               {
               unsigned char temp = data[index1];
               data[index1] = data[index2];
               data[index2] = temp;
               ++index1;
               ++index2;
               }
            }

         type = GL_UNSIGNED_BYTE;
         GLint maxTextureSize;
         glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
         glTexStorage2D(dim, intLog2(min(w, h)), iFormat, w, h);
         GLuint er = glGetError();
         glTexSubImage2D(dim, 0, 0, 0, w, h, format, type, data);
         glGenerateMipmap(dim);
         SOIL_free_image_data(data);
         er = glGetError();
         if(er != GL_NO_ERROR)
            EMP_DEVICE_ERROR(String("Failed to create texture!\nError code: "));
         }


      void APITexture<RS_GL43>::bindToVertex(uint32 ind) const
         {
         //Fill for GL component
         }

      void APITexture<RS_GL43>::bindToGeometry(uint32 ind) const
         {
         //Fill for GL component
         }

      void APITexture<RS_GL43>::bindToFragment(uint32 ind) const
         {
         //Fill for GL component
         }

      // size here is number of texels
      void APITexture1D<RS_GL43>::init(uint32 size, StructureFormat fmt)
         {
         //Fill for GL component
         //use _glTranslateFormat to convert (fmt) into (iFormat), (format)
         //and (type)
         //Use EMP_DEVICE_ERROR to emit exception if gl error occcurs
         }

      // size here is byte size
      void APITexture1D<RS_GL43>::fill(byte* data, uint32 size)
         {
         //Bind texture and copy in data using gl functions
         //width should be (size) / 16
         //Use EMP_DEVICE_ERROR to emit exception if gl error occcurs
         }

      void APITexture2D<RS_GL43>::init(uint32 width, uint32 height, StructureFormat fmt)
         {
         //Fill for GL component
         //use _glTranslateFormat to convert (fmt) into (iFormat), (format)
         //and (type)
         //Use EMP_DEVICE_ERROR to emit exception if gl error occcurs
         }

      void APITexture2D<RS_GL43>::fill(byte* data, uint32 size)
         {
         //Bind texture and copy in data using gl functions
         //width and height should match preset values in current object
         //Use EMP_DEVICE_ERROR to emit exception if gl error occcurs
         }






      APIRenderTarget<RS_GL43>::~APIRenderTarget()
         {
         //Fill Me
         }

      void APIRenderTarget<RS_GL43>::init(uint32 width, uint32 height, StructureFormat fmt)
         {
         //Fill Me
         }




      APIDepthStencil<RS_GL43>::~APIDepthStencil()
         {
         //Fill Me
         }

      void APIDepthStencil<RS_GL43>::init(uint32 width, uint32 height, StructureFormat fmt)
         {
         //Fill Me
         }








      void APITexture3D<RS_GL43>::init(uint32 width, uint32 height, uint32 depth, StructureFormat fmt)
         {
         //Fill for GL component
         //use _glTranslateFormat to convert (fmt) into (iFormat), (format)
         //and (type)
         //Use EMP_DEVICE_ERROR to emit exception if gl error occcurs
         }

      void APITexture3D<RS_GL43>::fill(byte* data, uint32 size)
         {
         //Bind texture and copy in data using gl functions
         //width, height and depth should match preset values in current object
         //Use EMP_DEVICE_ERROR to emit exception if gl error occcurs
         }
#endif
      }
   }
