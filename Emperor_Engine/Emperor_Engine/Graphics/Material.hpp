#ifndef __EMP_GFX_MATERIAL_HPP__
#define __EMP_GFX_MATERIAL_HPP__

#include <Graphics/Types.hpp>
#include "InternalTypes.hpp"
#include "Texture.hpp"
#include "Mesh.hpp"
#include "Light.hpp"
#include "Buffers.hpp"
#include <Graphics/Vertex.hpp>
#include "BlendState.hpp"
#include "Shader.hpp"
#include <Graphics/iMaterial.hpp>
#include "APIProgram.hpp"
#include "ShaderManager.hpp"
#include "TextureSampler.hpp"
#include <Graphics/iVertexFormat.hpp>

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class PassBase
         {
         private:
         protected:
            ArrayList<const Texture<RS>*> textures;
            ArrayList<const TextureSampler<RS>*> samplers;
            ArrayList<const UniformBuffer<RS>*> uniforms;

            bool ready;
         public:
            PassBase() : ready(false) {}
            void addTextureReference(const Texture<RS>* t)
               { textures.push_back(t); }
            void addSamplerReference(const TextureSampler<RS>* s)
               { samplers.push_back(s); }
            void addUniformRefernce(const UniformBuffer<RS>* u)
               { uniforms.push_back(u); }
            void setShader(const String& fileName,
               const String& functionName);
            bool isReady() { return ready; }
         };

      template <RenderSystem RS>
      class PassVertex : public PassBase<RS>
         {
         private:
            VertexShader<RS>* vs;
         protected:
         public:
            PassVertex() : vs(0) {}
            void setShader(const String& fileName,
               const String& functionName, const iVertexFormat& vd);
            void bind() const;
            uint32 _exposeID() { return vs->_exposeID(); }
         };

      template <RenderSystem RS>
      class PassGeometry : public PassBase<RS>
         {
         private:
            GeometryShader<RS>* gs;
         protected:
         public:
            PassGeometry() : gs(0) {}
            void setShader(const String& fileName, const String& functionName);
            void disable()
               {
               gs = ShaderManager<RS>::getPtr()->getGeometryShader(Defaults::mapName, Defaults::mapName);
               ready = true;
               }
            void bind() const;
            uint32 _exposeID() { return gs->_exposeID(); }
         };

      template <RenderSystem RS>
      class PassFragment : public PassBase<RS>
         {
         private:
            FragmentShader<RS>* fs;
         protected:
         public:
            PassFragment() : fs(0) {}
            void setShader(const String& fileName, const String& functionName);
            void bind() const;
            uint32 _exposeID() { return fs->_exposeID(); }
         };

      template <RenderSystem RS>
      class MaterialPass
         {
         private:
            PassVertex<RS> pv;
            PassGeometry<RS> pg;
            PassFragment<RS> pf;
            APIProgram<RS> program;

            BlendState<RS>* blend;
         protected:
         public:
            MaterialPass() : blend(0) {}
            ~MaterialPass() {}

            PassVertex<RS>& vertex() { return pv; }
            PassGeometry<RS>& geometry() { return pg; }
            PassFragment<RS>& fragment() { return pf; }

            void finalize();

            void setBlendState(const String&);

            void bindPass() const;

            void(*matFunc)(const UniformBuffer<RS>&, uint32);
            void(*actFunc)(const UniformBuffer<RS>&, uint32);
            void(*camFunc)(const UniformBuffer<RS>&, uint32);
            void(*lgtFunc)(const UniformBuffer<RS>&, uint32);
         };

      template <RenderSystem RS>
      class MaterialTechnique
         {
         private:
            ArrayList<Pair<MaterialPass<RS>, MaterialPassDescription>> passes;
            uint32 LOD;
            uint32 quality;
            uint32 id;

            UniformBuffer<RS>* material;

         protected:
         public:
            MaterialTechnique() : LOD(0), quality(0), material(0), id(0) {}

            MaterialTechnique(uint32 i, uint32 l, uint32 q, UniformBuffer<RS>* m) :
               id(i), LOD(l), quality(q), material(m) {}

            void addPass(const MaterialPassDescription& desc);

            void setLOD(uint32 l) { LOD = l; }
            uint32 getLOD() const { return LOD; }

            void setQuality(uint32 q) { quality = q; }
            uint32 getQuality() const { return quality; }

            void setID(uint32 q) { id = q; }
            uint32 getID() const { return id; }

            void render(uint32) const;

            void _setupBuffers(UniformBuffer<RS>* m)
               {
               material = m;
               }
         };


      template <RenderSystem RS>
      class Material : public iMaterial
         {
         private:
            ArrayList<MaterialTechnique<RS>> techniques;
            ArrayList<MaterialPassDescription> passDefs;

            uint32 matIndex;



            UniformBuffer<RS> material;
         protected:
            virtual ~Material() {}
         public:
            Material() : matIndex(0) {}

            uint32 addSize(uint32 typeSize);

            void setValue(uint32, void*);

            uint32 createTechnique(uint32 id, uint32 quality, uint32 LOD);

            uint32 createPass(const MaterialPassDescription&);

            void addPassToTechnique(uint32 tech, uint32 pass);


            void draw(uint32 id, uint32 quality, uint32 lod, uint32 indexCount) const;

            void destroy();
         };
      }
   }

#endif