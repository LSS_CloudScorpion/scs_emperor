#ifndef __EMP_GFX_RESOURCE_CONTROLLER_HPP__
#define __EMP_GFX_RESOURCE_CONTROLLER_HPP__

#include <Graphics/iResourceController.hpp>
#include "Managers.hpp"
#include "ShaderManager.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class ResourceController : public iResourceController
         {
         private:
         protected:
         public:
            VertexFormatManager<RS> vfmMan;
            MeshManager<RS> mshMan;
            MaterialManager<RS> matMan;
            TextureManager<RS> texMan;
            SamplerManager<RS> smpMan;
            BlendStateManager<RS> blnMan;
            ShaderManager<RS> shdMan;

            //LAB: Should call the getResource function on the appropriate 
            //manager, passing in the name (n)

            const iVertexFormat* getVertexFormat(const LString& n)
               { return new VertexFormat<RS>; }
            const iMesh* getMesh(const LString& n)
               { return new Mesh<RS>; }
            const iMaterial* getMaterial(const LString& n)
               { return new Material<RS>; }
            const iTexture* getTexture(const LString& n)
               { return new Texture<RS>; }
            const iTextureSampler* getSampler(const LString& n)
               { return new TextureSampler<RS>; }
            const iBlendState* getBlendState(const LString& n)
               { return new BlendState<RS>; }

            //LAB: Calls the appropriate object creation function on the 
            //appropriate manager, passing in the name (n)

            iVertexFormat* newVertexFormat(const LString& n)
               { return new VertexFormat<RS>; }
            iMesh* newCustomMesh(const LString& n)
               { return new Mesh<RS>; }
            iMaterial* newCustomMaterial(const LString& n)
               { return new Material<RS>; }
            iTexture1D* newCustomTexture1D(const LString& n)
               { return new Texture1D<RS>; }
            iTexture2D* newCustomTexture2D(const LString& n)
               { return new Texture2D<RS>; }
            iTexture3D* newCustomTexture3D(const LString& n)
               { return new Texture3D<RS>; }
            iTexture2D* newCustomRenderTarget(const LString& n)
               { return new RenderTarget<RS>; }
            iTexture2D* newCustomDepthStencil(const LString& n)
               { return new DepthStencil<RS>; }
            iTextureSampler* newCustomSampler(const LString& n)
               { return new TextureSampler<RS>; }
            iBlendState* newBlendState(const LString& n)
               { return new BlendState<RS>; }
         };
      }
   }

#endif