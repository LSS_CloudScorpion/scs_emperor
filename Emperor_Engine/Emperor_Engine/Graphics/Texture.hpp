#ifndef __EMP_GFX_TEXTURE_HPP__
#define __EMP_GFX_TEXTURE_HPP__

#include "InternalTypes.hpp"
#include <Graphics/iTexture.hpp>
#include "APITexture.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class Texture : public iTexture
         {
         private:
         protected:
            APITexture<RS>* texture;
            TextureType type;
            ~Texture() { delete texture; }
         public:
            Texture() : texture(new APITexture<RS>()), type(TT_2D) {}
            Texture(APITexture<RS>* a) : texture(a) {}

            void loadFile(const LString&);
            void loadFromMem(byte*, uint32);
            void destroy();
            TextureType getType() { return type; }

            void bindToVertex(uint32 slot) const;
            void bindToGeometry(uint32 slot) const;
            void bindToFragment(uint32 slot) const;
         };

      template <RenderSystem RS>
      class Texture1D : public iTexture1D, public Texture<RS>
         {
         private:
            APITexture1D<RS> tex1d;
            uint32 texSize;
         protected:
            ~Texture1D() {texture = 0;}
         public:
            Texture1D() : texSize(0) { type = TT_1D; texture = 
               (APITexture<RS>*)&tex1d; }
            void initCustom(uint32 size, StructureFormat format);
            void fill(byte*, uint32);
         };

      template <RenderSystem RS, class T>
      class _Texture2D : public iTexture2D, public Texture<RS>
         {
         private:
         protected:
            T tex2d;
            uint32 texSize;
            ~_Texture2D() { texture = 0; }
         public:
            _Texture2D() : texSize(0) { texture = (APITexture<RS>*)&tex2d; }
            void initCustom(uint32 width, uint32 height, 
               StructureFormat format);
            void fill(byte*, uint32);
            const T* _exposeTexture() const { return &tex2d; }
         };

      template <RenderSystem RS>
      class Texture2D : public _Texture2D<RS, APITexture2D<RS>> 
         { public: Texture2D() { type = TT_2D; } };

      template <RenderSystem RS>
      class RenderTarget : public _Texture2D<RS, APIRenderTarget<RS>> 
         { public: RenderTarget() { type = TT_RENDER_TARGET; } };

      template <RenderSystem RS>
      class DepthStencil : public _Texture2D<RS, APIDepthStencil<RS>> 
         { public: DepthStencil() { type = TT_DEPTH_STENCIL; } };

      template <RenderSystem RS>
      class Texture3D : public iTexture3D, public Texture<RS>
         {
         private:
            APITexture3D<RS> tex3d;
            uint32 texSize;
            ~Texture3D() { texture = 0; }
         protected:
         public:
            Texture3D() : texSize(0) { type = TT_3D; texture = 
               (APITexture<RS>*)&tex3d; }
            void initCustom(uint32 width, uint32 height, uint32 depth, 
               StructureFormat format);
            void fill(byte*, uint32);
         };
      }
   }


#endif