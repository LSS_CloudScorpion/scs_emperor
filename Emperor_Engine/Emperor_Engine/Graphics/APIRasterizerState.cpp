#include "APIRasterizerState.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_DIRECTX
      APIRasterizerState<RS_DX11>::~APIRasterizerState()
         {
         if(rasterState)
            rasterState->Release();
         }

      void APIRasterizerState<RS_DX11>::init(const RasterizerStateDescription& r)
         {
         //Create a new DX11 rasterizer desc and fill it with the information
         //in (r), converting where nessisary (check APITranslation.hpp)
         //Use EMP_DEVICE_ASSERT_DX when creating the rasterizer state
         }

      void APIRasterizerState<RS_DX11>::bind()
         {
         //Fill Me
         //Note: use singleton to expose context
         }
#endif

#if EMP_USE_OPENGL
      inline void _glEnProx(GLuint a)
         {
         glEnable(a);
         }

      inline void _glDsProx(GLuint a)
         {
         glDisable(a);
         }

      void APIRasterizerState<RS_GL43>::init(const RasterizerStateDescription& r)
         {
         //Fill for GL component

         //for some attributes, check if the associated value in (r) is true,
         //if so, assign _glEnProx, else assign _glDsProx. aaLine is provided
         //as an example. This only applies to attributes which are function
         //pointers.
         aaLine = r.antialiasedLineEnabled ? _glEnProx : _glDsProx;
         
         }

      void APIRasterizerState<RS_GL43>::bind()
         {
         //Fill for GL component

         //for function pointer attributes, call the function and pass in the
         //associated #define. aaLine provided as an example. All other
         //attributes must call correct gl functions
         aaLine(GL_LINE_SMOOTH);
         }

      APIRasterizerState<RS_GL43>::APIRasterizerState()
         {//defaults
         aaLine = _glDsProx;
         cullFace = _glDsProx;
         cullDir = GL_FRONT;
         depthBias = 0;
         depthClamp = 0;
         fillMode = GL_FILL;
         windOrder = GL_CCW;
         multiSample = _glDsProx;
         scissorTest = _glDsProx;
         depthClip = _glDsProx;
         }
#endif
      }
   }
