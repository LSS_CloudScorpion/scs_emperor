#ifndef __EMP_GFX_MANAGERS_HPP__
#define __EMP_GFX_MANAGERS_HPP__

#include "../BaseManager.hpp"
#include "../ResourceManager.hpp"
#include "Actor.hpp"
#include "Buffers.hpp"
#include "Camera.hpp"
#include "Light.hpp"
#include "VertexFormat.hpp"
#include "Mesh.hpp"
#include "Material.hpp"
#include "Texture.hpp"
#include "../Node.hpp"
#include "../Utilities/Window.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class NodeManager
         {
         private:
            Texture1D<RS>* nodeBuffer;
            TextureSampler<RS>* nodeSampler;
         protected:
         public:
            NodeManager() {}
            virtual ~NodeManager() {}

            void init();

            void _updateBuffers();
            void _bindBuffers();
         };


      template <RenderSystem RS>
      class ActorManager : public Singleton<ActorManager<RS>>, public IterativeManager<Actor<RS>>
         {
         private:
         protected:
         public:
            ActorManager() {}
            virtual ~ActorManager() {}

            bool bindNext();
            void render();
         };


      template <RenderSystem RS>
      class CameraManager : public Singleton<CameraManager<RS>>, public IterativeManager<Camera<RS>>
         {
         private:
            Matrix<float, 4> view;
         protected:
         public:
            CameraManager() {}
            virtual ~CameraManager() {}

            bool bindNext();
            void orderCameras();
            Matrix<float, 4> getView() const { return view; }
         };

      template <RenderSystem RS>
      class LightManager : public Singleton<LightManager<RS>>, public IterativeManager<Light<RS>>
         {
         private:
         protected:
            static Vector<float, 3> compPoint;
            static bool lightCompare(Light<RS>* a, Light<RS>* b);
            TextureSampler<RS>* shadowSampler;
         public:
            LightManager() {}
            virtual ~LightManager() {}

            void orderLights(const Vector<float, 3>&);
            void prepareLoop() { it = activeObjects.begin(); /*shadowSampler->bindToFragment(EMP_SHADOW_TEX_SLOT);*/ }

            void init();
            bool bindNext(const Vector<float, 3>&, bool shadows = false);
            bool bindNext(const Vector<float, 3>&, LightType, bool shadows = false);
         };

      template <RenderSystem RS>
      class VertexFormatManager : public Singleton<VertexFormatManager<RS> >, public ResourceManager<VertexFormat<RS>>
         {
         private:
         protected:
            VertexFormat<RS>* createResource(const String& r);
         public:
            void init();

            VertexFormatManager() {}
            virtual ~VertexFormatManager() {}
         };

      template <RenderSystem RS>
      class MeshManager : public Singleton<MeshManager<RS>>, public ResourceManager<Mesh<RS>>
         {
         private:
         protected:
            Mesh<RS>* createResource(const String& r);
         public:
            void init();
            void resolveLoad(byte*, uint32, Mesh<RS>*);
            MeshManager() {}
            virtual ~MeshManager() {}
         };


      template <RenderSystem RS>
      class MaterialManager : public Singleton<MaterialManager<RS>>, public ResourceManager<Material<RS>>
         {
         private:
         protected:
            Material<RS>* createResource(const String& r);
         public:
            MaterialManager() {}
            void init();
            void resolveLoad(byte*, uint32, Material<RS>*);
            virtual ~MaterialManager() {}
         };

      template <RenderSystem RS>
      class TextureManager : public Singleton<TextureManager<RS>>, public ResourceManager<Texture<RS>>
         {
         private:
         protected:
            Texture<RS>* createResource(const String& r);
            void resolveLoad(byte* d, uint32 s, Texture<RS>* o);
         public:
            TextureManager() {}
            virtual ~TextureManager() {}


            Texture1D<RS>* newTex1D(const String&);
            Texture2D<RS>* newTex2D(const String&);
            Texture3D<RS>* newTex3D(const String&);
            RenderTarget<RS>* newTarget(const String&);
            DepthStencil<RS>* newDepth(const String&);
            void init();
         };

      template <RenderSystem RS>
      class SamplerManager : public Singleton<SamplerManager<RS>>, public ResourceManager<TextureSampler<RS>>
         {
         private:
         protected:
            TextureSampler<RS>* createResource(const String& r);
         public:
            SamplerManager() {}
            virtual ~SamplerManager() {}
         };


      template <RenderSystem RS>
      class BlendStateManager : public Singleton<BlendStateManager<RS>>, public ResourceManager<BlendState<RS>>
         {
         private:
         protected:
            BlendState<RS>* createResource(const String& r);
         public:
            BlendStateManager() {}
            virtual ~BlendStateManager() {}
         };
      }
   }

#endif