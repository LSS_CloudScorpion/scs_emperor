#include "Buffers.hpp"
#include "Engine.hpp"
#include "VertexFormat.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      Buffer<RS>::~Buffer()
         {
         if(finalized)
            releaseBuffer();
         delete buffer;
         }

      template <RenderSystem RS>
      void Buffer<RS>::addSize(uint32 s)
         {
         if(finalized)
            {
            releaseBuffer();
            finalized = false;
            }
         dynamIndex.push_back(byteSize);
         for(uint32 j = 0; j < s; j++)
            content.push_back(0);
         byteSize += s;
         indexSize++;
         }

      template <RenderSystem RS>
      void Buffer<RS>::fill(const byte* b)
         {
         fill(b, byteSize);
         }

      template <RenderSystem RS>
      void Buffer<RS>::fill(const byte* b, uint32 size)
         {
         if(!finalized)
            {
            finalize();
            if(!finalized)
               {
               LOG("Failed to fill buffer!");
               return;
               }
            }
         for(uint32 i = 0; i < size; i++)
            content[i] = b[i];
         try
            {
            buffer->fill(b, size);
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

      template <RenderSystem RS>
      void VertexBuffer<RS>::finalize()
         {
         try
            {
            ((APIVertexBuffer<RS>*)buffer)->finalize(*vertDef, byteSize);
            finalized = true;
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

      template <RenderSystem RS>
      void IndexBuffer<RS>::finalize()
         {
         try
            {
            ((APIIndexBuffer<RS>*)buffer)->finalize(byteSize);
            finalized = true;
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

      template <RenderSystem RS>
      void UniformBuffer<RS>::finalize()
         {
         try
            {
            ((APIUniformBuffer<RS>*)buffer)->finalize(byteSize);
            finalized = true;
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

      template <RenderSystem RS>
      void VertexBuffer<RS>::bindVertex() const
         {
         ((APIVertexBuffer<RS>*)buffer)->bind(vertDef->getByteSize());
         }

      template <RenderSystem RS>
      void IndexBuffer<RS>::bindIndex() const
         {
         ((APIIndexBuffer<RS>*)buffer)->bind();
         }

      template <RenderSystem RS>
      void UniformBuffer<RS>::bindToVertex(uint32 slot) const
         {
         ((APIUniformBuffer<RS>*)buffer)->bindToVertex(slot);
         }

      template <RenderSystem RS>
      void UniformBuffer<RS>::bindToGeometry(uint32 slot) const
         {
         ((APIUniformBuffer<RS>*)buffer)->bindToGeometry(slot);
         }

      template <RenderSystem RS>
      void UniformBuffer<RS>::bindToFragment(uint32 slot) const
         {
         ((APIUniformBuffer<RS>*)buffer)->bindToFragment(slot);
         }


#if (EMP_USE_OPENGL == ON)
      template class Buffer<RS_GL43>;
      template class UniformBuffer<RS_GL43>;
      template class VertexBuffer<RS_GL43>;
      template class IndexBuffer<RS_GL43>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class Buffer<RS_DX11>;
      template class UniformBuffer<RS_DX11>;
      template class VertexBuffer<RS_DX11>;
      template class IndexBuffer<RS_DX11>;
#endif
      }
   }