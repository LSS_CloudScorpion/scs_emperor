#include "InternalTypes.hpp"
#include "ShaderManager.hpp"
#include "Managers.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      ShaderManager<RS>::ShaderManager()
         {}

      template <RenderSystem RS>
      void ShaderManager<RS>::init()
         {
         //construct safe default shader
         VertexShader<RS>* dv = 0;
         GeometryShader<RS>* dg = 0;
         FragmentShader<RS>* df = 0;

         dv = new VertexShader<RS>();
         dg = new GeometryShader<RS>();
         df = new FragmentShader<RS>();

         dv->associateVertex(
            *VertexFormatManager<RS>::getPtr()->getResource(
            Defaults::vertexFormatName));

         if(RS == RS_DX11)
            {
            dv->initFromDef(Defaults::shaderDefDX, Defaults::vertexFunc);
            //dg->initFromDef(Defaults::shaderDefDX, Defaults::geometryFunc);
            df->initFromDef(Defaults::shaderDefDX, Defaults::fragmentFunc);
            }
         else
            {
            dv->initFromDef(Defaults::shaderDefGL, Defaults::vertexFunc);
            //dg->initFromDef(Defaults::shaderDefGL, Defaults::geometryFunc);
            df->initFromDef(Defaults::shaderDefGL, Defaults::fragmentFunc);
            }

         vShaders[Defaults::mapName][Defaults::mapName] = dv;
         gShaders[Defaults::mapName][Defaults::mapName] = dg;
         fShaders[Defaults::mapName][Defaults::mapName] = df;
         }

      template <RenderSystem RS>
      ShaderManager<RS>::~ShaderManager()
         {
         for(auto i = vShaders.begin(); i != vShaders.end(); i++)
            {
            for(auto j = i->second.begin(); j != i->second.end(); j++)
               {
               j->second->destroy();
               }
            }

         for(auto i = gShaders.begin(); i != gShaders.end(); i++)
            {
            for(auto j = i->second.begin(); j != i->second.end(); j++)
               {
               j->second->destroy();
               }
            }

         for(auto i = fShaders.begin(); i != fShaders.end(); i++)
            {
            for(auto j = i->second.begin(); j != i->second.end(); j++)
               {
               j->second->destroy();
               }
            }
         }

      template <RenderSystem RS>
      VertexShader<RS>* ShaderManager<RS>::getVertexShader(
         const String& file, const String& function, const iVertexFormat& p)
         {
         VertexShader<RS>* out = 0;
         auto f = vShaders.find(file);
         if(f != vShaders.end())
            {
            auto fn = f->second.find(function);
            if(fn != f->second.end())
               {
               out = fn->second;
               }
            else
               {
               out = new VertexShader<RS>();
               out->associateVertex(p);
               try
                  {
                  out->Shader::init(file, function);
                  vShaders[file][function] = out;
                  }
               catch(DeviceFailureException e)
                  {
                  LOG(e.what());
                  out->destroy();
                  out = vShaders[Defaults::mapName][Defaults::mapName];
                  }
               }
            }
         else
            {
            vShaders[file] = HashMap<String, VertexShader<RS>*>();
            out = new VertexShader<RS>();
            out->associateVertex(p);
            try
               {
               out->Shader::init(file, function);
               vShaders[file][function] = out;
               }
            catch(DeviceFailureException e)
               {
               LOG(e.what());
               out->destroy();
               out = vShaders[Defaults::mapName][Defaults::mapName];
               }
            }
         return out;
         }

      template <RenderSystem RS>
      GeometryShader<RS>* ShaderManager<RS>::getGeometryShader(const String& file, const String& function)
         {
         GeometryShader<RS>* out = 0;
         auto f = gShaders.find(file);
         if(f != gShaders.end())
            {
            auto fn = f->second.find(function);
            if(fn != f->second.end())
               {
               out = fn->second;
               }
            else
               {
               out = new GeometryShader<RS>();
               try
                  {
                  out->Shader::init(file, function);
                  gShaders[file][function] = out;
                  }
               catch(DeviceFailureException e)
                  {
                  LOG(e.what());
                  out->destroy();
                  out = gShaders[Defaults::mapName][Defaults::mapName];
                  }
               }
            }
         else
            {
            gShaders[file] = HashMap<String, GeometryShader<RS>*>();
            out = new GeometryShader<RS>();
            try
               {
               out->Shader::init(file, function);
               gShaders[file][function] = out;
               }
            catch(DeviceFailureException e)
               {
               LOG(e.what());
               out->destroy();
               out = gShaders[Defaults::mapName][Defaults::mapName];
               }
            }
         return out;
         }


      template <RenderSystem RS>
      FragmentShader<RS>* ShaderManager<RS>::getFragmentShader(const String& file, const String& function)
         {
         FragmentShader<RS>* out = 0;
         auto f = fShaders.find(file);
         if(f != fShaders.end())
            {
            auto fn = f->second.find(function);
            if(fn != f->second.end())
               {
               out = fn->second;
               }
            else
               {
               out = new FragmentShader<RS>();
               try
                  {
                  out->Shader::init(file, function);
                  fShaders[file][function] = out;
                  }
               catch(DeviceFailureException e)
                  {
                  LOG(e.what());
                  out->destroy();
                  out = fShaders[Defaults::mapName][Defaults::mapName];
                  }
               }
            }
         else
            {
            fShaders[file] = HashMap<String, FragmentShader<RS>*>();
            out = new FragmentShader<RS>();
            try
               {
               out->Shader::init(file, function);
               fShaders[file][function] = out;
               }
            catch(DeviceFailureException e)
               {
               LOG(e.what());
               out->destroy();
               out = fShaders[Defaults::mapName][Defaults::mapName];
               }
            }
         return out;
         }

      template <RenderSystem RS>
      void ShaderManager<RS>::preloadVertexShaders(const ArrayList<Pair<String, String>>& r)
         {
         auto end = r.end();
         for(auto i = r.begin(); i < end; i++)
            {
            auto v = new VertexShader<RS>();
            v->Shader::init(i->first, i->second);
            vShaders[i->first][i->second] = v;
            }
         }

      template <RenderSystem RS>
      void ShaderManager<RS>::preloadGeometryShaders(const ArrayList<Pair<String, String>>& r)
         {
         auto end = r.end();
         for(auto i = r.begin(); i < end; i++)
            {
            auto f = new GeometryShader<RS>();
            f->Shader::init(i->first, i->second);
            gShaders[i->first][i->second] = f;
            }
         }

      template <RenderSystem RS>
      void ShaderManager<RS>::preloadFragmentShaders(const ArrayList<Pair<String, String>>& r)
         {
         auto end = r.end();
         for(auto i = r.begin(); i < end; i++)
            {
            auto f = new FragmentShader<RS>();
            f->Shader::init(i->first, i->second);
            fShaders[i->first][i->second] = f;
            }
         }

#if (EMP_USE_OPENGL == ON)
      template class ShaderManager<RS_GL43>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class ShaderManager<RS_DX11>;
#endif
      }
   }