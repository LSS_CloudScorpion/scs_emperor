#ifndef __EMP_GFX_MATERIAL_PARSER_HPP__
#define __EMP_GFX_MATERIAL_PARSER_HPP__

#include "InternalTypes.hpp"
#include <json\json.h>

#define _EMP_PARSE_FIND_(CAM, LGT, ACT, MAT) (*&(type == "camera" ? \
   ps.CAM : type == "light" ? ps.LGT : type == "actor" ? ps.ACT : ps.MAT))

#define _EMP_GET_MPDO_(v,g,f) v ? g ? f ? MPDO_VGF : MPDO_VG : f ? MPDO_VF : MPDO_VERTEX\
   : g ? f ? MPDO_GF : MPDO_GEOMETRY : f ? MPDO_FRAGMENT : MPDO_NONE

namespace Emperor
   {
   namespace Graphics
      {
      typedef Json::Value Jval;

      template <RenderSystem RS>
      class Material;

      struct _techDesc
         {
         uint32 id, quality, LOD;
         ArrayList<String> passes;
         };

      struct _sampDesc
         {
         TextureFilter filter;
         Color bc;
         TextureComparisonMethod cm;
         uint32 maxAni;
         float maxLOD;
         float minLOD;
         float mipBias;
         TextureAddressMode u;
         TextureAddressMode v;
         TextureAddressMode w;

         _sampDesc() : bc(0, 0, 0, 1), cm(TC_NEVER), maxAni(1), maxLOD(float(0xFFFFFFFF)),
            minLOD(0.0f), mipBias(0.0f), u(TA_CLAMP), v(TA_CLAMP), w(TA_CLAMP) {}
         };

      struct _vertFormat
         {
         ArrayList<String> types;
         ArrayList<String> syms;
         };

      struct _matDesc
         {
         ArrayList<String> techs;
         ArrayList<String> types;
         ArrayList<ArrayList<float>> vals;
         String nmsp;
         };

      void _parseMaterialFile(const String& file, const String& path,
         _matDesc& mat, HashMap<String, MaterialPassDescription>& passes,
         HashMap<String, _techDesc>& techs,
         HashMap<String, BlendStateDescription>& blends,
         HashMap<String, _sampDesc>& samps,
         HashMap<String, _vertFormat>& fmts,
         HashMap<String, String>& or);

      void _parseMaterialDef(const String& file, const String& def, 
         const String& path,
         _matDesc& mat, HashMap<String, MaterialPassDescription>& passes,
         HashMap<String, _techDesc>& techs,
         HashMap<String, BlendStateDescription>& blends,
         HashMap<String, _sampDesc>& samps,
         HashMap<String, _vertFormat>& fmts,
         HashMap<String, String>& or);


      template <RenderSystem RS>
      Material<RS>* _buildMaterial(const String& r, Material<RS>* o = 0, const String& def = String());
      }
   }
#endif