#ifndef __EMP_GFX_SCENE_OBJECT_HPP__
#define __EMP_GFX_SCENE_OBJECT_HPP__

#include "../SceneObject.hpp"
#include "Buffers.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS, class I = iSceneObject>
      class SceneObject : public Emperor::SceneObject<I>
         {
         private:
         protected:
            UniformBuffer<RS> buffer;
         public:
            inline UniformBuffer<RS>& _getBuffer() { return buffer; };
            inline void _bindVertex(uint32 slot) { buffer.bindToVertex(slot); }
            inline void _bindGeometry(uint32 slot) { buffer.bindToGeometry(slot); }
            inline void _bindFragment(uint32 slot) { buffer.bindToFragment(slot); }
         };
      }
   }
#endif