#ifndef __EMP_GFX_SHADER_MANAGER_HPP__
#define __EMP_GFX_SHADER_MANAGER_HPP__

#include "InternalTypes.hpp"
#include "Shader.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class ShaderManager : public Singleton<ShaderManager<RS>>
         {
         private:
            HashMap<String, HashMap<String, VertexShader<RS>*>> vShaders;
            HashMap<String, HashMap<String, GeometryShader<RS>*>> gShaders;
            HashMap<String, HashMap<String, FragmentShader<RS>*>> fShaders;
         protected:
         public:
            ShaderManager();
            ~ShaderManager();

            VertexShader<RS>* getVertexShader(const String& file,
               const String& function, const iVertexFormat&);
            GeometryShader<RS>* getGeometryShader(const String& file,
               const String& function);
            FragmentShader<RS>* getFragmentShader(const String& file,
               const String& function);

            void init();

            void preloadVertexShaders(const ArrayList<Pair<String, String>>&);
            void preloadGeometryShaders(const ArrayList<Pair<String, String>>&);
            void preloadFragmentShaders(const ArrayList<Pair<String, String>>&);
         };
      }
   }
#endif