#include "Material.hpp"
#include "Engine.hpp"
#include "Managers.hpp"
#include "ShaderManager.hpp"
#include <Graphics/iVertexFormat.hpp>

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      void PassVertex<RS>::setShader(const String& f, const String& fn,
         const iVertexFormat& vd)
         {
         vs = ShaderManager<RS>::getPtr()->getVertexShader(f, fn, vd);
         ready = true;
         }

      template <RenderSystem RS>
      void PassGeometry<RS>::setShader(const String& f, const String& fn)
         {
         gs = ShaderManager<RS>::getPtr()->getGeometryShader(f, fn);
         ready = true;
         }

      template <RenderSystem RS>
      void PassFragment<RS>::setShader(const String& f, const String& fn)
         {
         fs = ShaderManager<RS>::getPtr()->getFragmentShader(f, fn);
         ready = true;
         }

      template <RenderSystem RS>
      void PassVertex<RS>::bind() const
         {
         vs->bindShader();

         for(auto i = textures.begin(), end = textures.end(); i < end; i++)
            (*i)->bindToVertex(i - textures.begin() + 1);

         for(auto i = samplers.begin(), end = samplers.end(); i < end; i++)
            (*i)->bindToVertex(i - samplers.begin() + 1);

         for(auto i = uniforms.begin(), end = uniforms.end(); i < end; i++)
            (*i)->bindToVertex(i - uniforms.begin());
         }

      template <RenderSystem RS>
      void PassGeometry<RS>::bind() const
         {
         gs->bindShader();

         for(auto i = textures.begin(), end = textures.end(); i < end; i++)
            (*i)->bindToGeometry(i - textures.begin() + 1);

         for(auto i = samplers.begin(), end = samplers.end(); i < end; i++)
            (*i)->bindToGeometry(i - samplers.begin() + 1);

         for(auto i = uniforms.begin(), end = uniforms.end(); i < end; i++)
            (*i)->bindToGeometry(i - uniforms.begin());
         }

      template <RenderSystem RS>
      void PassFragment<RS>::bind() const
         {
         fs->bindShader();

         for(auto i = textures.begin(), end = textures.end(); i < end; i++)
            (*i)->bindToFragment(i - textures.begin() + 1);

         for(auto i = samplers.begin(), end = samplers.end(); i < end; i++)
            (*i)->bindToFragment(i - samplers.begin() + 1);

         for(auto i = uniforms.begin(), end = uniforms.end(); i < end; i++)
            (*i)->bindToFragment(i - uniforms.begin());
         }

      template <RenderSystem RS>
      void MaterialPass<RS>::finalize()
         {

         if(pv.isReady() && pg.isReady() && pf.isReady())
            {
            program.init(pv._exposeID(), pg._exposeID(), pf._exposeID());
            }
         else
            {
            LOG("Material pass cannot be finalized unless vertex, geometry and fragment shader are set, aborting...");
            }
         }

      template <RenderSystem RS>
      void MaterialPass<RS>::setBlendState(const String& r)
         {
         blend = BlendStateManager<RS>::getPtr()->getResource(r);
         }

      template <RenderSystem RS>
      void MaterialPass<RS>::bindPass() const
         {
         program.bind();
         pv.bind();
         pg.bind();
         pf.bind();

         blend->bindBlendState();
         }

      //////////////////////////////////////////////////////////////////////////////////////////////

      template <RenderSystem RS>
      static void _BindV(const UniformBuffer<RS>& a, uint32 b)
         {
         a.bindToVertex(b);
         }

      template <RenderSystem RS>
      static void _BindVG(const UniformBuffer<RS>& a, uint32 b)
         {
         a.bindToVertex(b);
         a.bindToGeometry(b);
         }

      template <RenderSystem RS>
      static void _BindVF(const UniformBuffer<RS>& a, uint32 b)
         {
         a.bindToVertex(b);
         a.bindToFragment(b);
         }

      template <RenderSystem RS>
      static void _BindVGF(const UniformBuffer<RS>& a, uint32 b)
         {
         a.bindToVertex(b);
         a.bindToGeometry(b);
         a.bindToFragment(b);
         }

      template <RenderSystem RS>
      static void _BindG(const UniformBuffer<RS>& a, uint32 b)
         {
         a.bindToGeometry(b);
         }

      template <RenderSystem RS>
      static void _BindGF(const UniformBuffer<RS>& a, uint32 b)
         {
         a.bindToGeometry(b);
         a.bindToFragment(b);
         }

      template <RenderSystem RS>
      static void _BindF(const UniformBuffer<RS>& a, uint32 b)
         {
         a.bindToFragment(b);
         }

      template <RenderSystem RS>
      static void _BindNone(const UniformBuffer<RS>& a, uint32 b)
         {}

      template <RenderSystem RS>
      static void _setfp(void(*&matFunc)(const UniformBuffer<RS>&, uint32), MaterialPassDescriptionOptions a)
         {
         switch(a)
            {
            case MPDO_NONE:
               matFunc = _BindNone;
               break;
            case MPDO_VERTEX:
               matFunc = _BindV;
               break;
            case MPDO_GEOMETRY:
               matFunc = _BindG;
               break;
            case MPDO_VG:
               matFunc = _BindVG;
               break;
            case MPDO_FRAGMENT:
               matFunc = _BindF;
               break;
            case MPDO_VF:
               matFunc = _BindVF;
               break;
            case MPDO_GF:
               matFunc = _BindGF;
               break;
            case MPDO_VGF:
               matFunc = _BindVGF;
               break;
            }
         }

      /////////////////////////////////////////////////////////////////////////

      template <RenderSystem RS>
      void MaterialTechnique<RS>::addPass(const MaterialPassDescription& desc)
         {
         MaterialPass<RS> mp;
         mp.vertex().setShader(desc.vertexFilename.stdString(), 
            desc.vertexFunction.stdString(),
            *VertexFormatManager<RS>::getPtr()->getResource(
            desc.vertexFormat.stdString())
            );
         if(desc.geometryFilename.stdString() != String(""))
            mp.geometry().setShader(desc.geometryFilename.stdString(), 
            desc.geometryFunction.stdString());
         else
            mp.geometry().disable();
         mp.fragment().setShader(desc.fragmentFilename.stdString(), 
            desc.fragmentFunction.stdString());
         auto samplers = desc.samplers.stdVector();
         for(auto i = samplers.begin(), end = samplers.end(); i < end; i++)
            {
            auto op = desc.samplerOptions[i - samplers.begin()];
            if(op & MPDO_VERTEX)
               mp.vertex().addSamplerReference(SamplerManager<RS>::getPtr()->getResource(i->stdString()));
            if(op & MPDO_GEOMETRY)
               mp.geometry().addSamplerReference(SamplerManager<RS>::getPtr()->getResource(i->stdString()));
            if(op & MPDO_FRAGMENT)
               mp.fragment().addSamplerReference(SamplerManager<RS>::getPtr()->getResource(i->stdString()));
            }

         auto textures = desc.textures.stdVector();
         for(auto i = textures.begin(), end = textures.end(); i < end; i++)
            {
            auto op = desc.textureOptions[i - textures.begin()];
            if(op & MPDO_VERTEX)
               mp.vertex().addTextureReference(TextureManager<RS>::getPtr()->getResource(i->stdString()));
            if(op & MPDO_GEOMETRY)
               mp.geometry().addTextureReference(TextureManager<RS>::getPtr()->getResource(i->stdString()));
            if(op & MPDO_FRAGMENT)
               mp.fragment().addTextureReference(TextureManager<RS>::getPtr()->getResource(i->stdString()));
            }

         mp.setBlendState(desc.blendState.stdString());
         mp.finalize();

         _setfp(mp.matFunc, desc.useMaterialBuffer);
         _setfp(mp.actFunc, desc.useActor);
         _setfp(mp.camFunc, desc.useCamera);
         _setfp(mp.lgtFunc, desc.useSingleLight);

         passes.push_back(Pair<MaterialPass<RS>, MaterialPassDescription>(mp, desc));
         }

      template <RenderSystem RS>
      void MaterialTechnique<RS>::render(uint32 indexCount) const
         {

         for(auto i = passes.begin(), end = passes.end(); i < end; i++)
            {
            (*i).first.bindPass();

            auto& desc = (*i).second;

            ((SceneController<RS>*)Engine<RS>::getPtr()->getSceneController())->nodMan._bindBuffers();

            i->first.matFunc(*material, desc.materialSlot);
            i->first.actFunc(ActorManager<RS>::getPtr()->getCurrent()->_getBuffer(), desc.actorSlot);
            i->first.camFunc(CameraManager<RS>::getPtr()->getCurrent()->_getBuffer(), desc.cameraSlot);

            if(desc.loopThroughLights)
               {
               LightManager<RS>& lMan = *LightManager<RS>::getPtr();
               ActorManager<RS>& aMan = *ActorManager<RS>::getPtr();
               lMan.prepareLoop();
               if(desc.loopThroughOneLightType)
                  {
                  while(lMan.bindNext(aMan.getCurrent()->getNode()->_getAbsTransform()[3].xyz(), desc.lightLoopType, desc.loopThroughShadowLights))
                     {
                     i->first.lgtFunc(lMan.getCurrent()->_getBuffer(), desc.singleLightSlot);
                     if(i->second.useShadowTexture && lMan.getCurrent()->castsShadows())
                        lMan.getCurrent()->_bindShadow(EMP_SHADOW_TEX_SLOT);
                     int c = (*i).second.loopCount;
                     while(c--)
                        {
                        _renderFromIndex<RS>(indexCount);
                        }
                     }
                  }
               else
                  {
                  while(lMan.bindNext(aMan.getCurrent()->getNode()->_getAbsTransform()[3].xyz(), desc.loopThroughShadowLights))
                     {
                     i->first.lgtFunc(lMan.getCurrent()->_getBuffer(), desc.singleLightSlot);
                     if(i->second.useShadowTexture && lMan.getCurrent()->castsShadows())
                        lMan.getCurrent()->_bindShadow(EMP_SHADOW_TEX_SLOT);
                     int c = (*i).second.loopCount;
                     while(c--)
                        {
                        _renderFromIndex<RS>(indexCount);
                        }
                     }
                  }
               }
            else
               {
               int c = desc.loopCount;
               while(c--)
                  _renderFromIndex<RS>(indexCount);
               }
            }

         }

      template <RenderSystem RS>
      uint32 Material<RS>::addSize(uint32 typeSize)
         {
         material.addSize(typeSize);
         return matIndex++;
         }

      template <RenderSystem RS>
      void Material<RS>::setValue(uint32 i, void* d)
         {
         material.setData(i, d);
         }

      template <RenderSystem RS>
      uint32 Material<RS>::createTechnique(uint32 id, uint32 quality, uint32 LOD)
         {
         techniques.push_back(MaterialTechnique<RS>(id, LOD, quality, &material));
         return techniques.size() - 1;
         }

      template <RenderSystem RS>
      uint32 Material<RS>::createPass(const MaterialPassDescription& mpd)
         {
         passDefs.push_back(mpd);
         return passDefs.size() - 1;
         }

      template <RenderSystem RS>
      void Material<RS>::addPassToTechnique(uint32 t, uint32 p)
         {
         techniques[t].addPass(passDefs[p]);
         }

      template <RenderSystem RS>
      void Material<RS>::draw(uint32 id, uint32 quality, uint32 lod, uint32 indexCount) const
         {
         for(auto i = techniques.begin(), end = techniques.end(); i < end; i++)
            {
            if((*i).getID() == id && (*i).getLOD() == lod && (*i).getQuality() == quality)
               {
               (*i).render(indexCount);
               break;
               }
            }

         }

      template <RenderSystem RS>
      void Material<RS>::destroy()
         {
         MaterialManager<RS>::getPtr()->removeResource(this);
         delete this;
         }

#if (EMP_USE_OPENGL == ON)
      template class Material<RS_GL43>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class Material<RS_DX11>;
#endif
      }
   }
