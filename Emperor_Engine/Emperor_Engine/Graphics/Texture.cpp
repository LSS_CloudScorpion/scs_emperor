#include "Texture.hpp"
#include "Engine.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      void Texture<RS>::loadFile(const LString& s)
         {
         texture->loadFile(Engine<RS>::getPtr()->getResourceFolder().stdString()
            + s.stdString());
         }

      template <RenderSystem RS>
      void Texture<RS>::loadFromMem(byte* m, uint32 s)
         {
         texture->loadFromMem(m,s);
         }

      template <RenderSystem RS>
      void Texture<RS>::destroy()
         {
         TextureManager<RS>::getPtr()->removeResource(this);
         delete this;
         }

      template <RenderSystem RS>
      void Texture<RS>::bindToVertex(uint32 slot) const
         {
         texture->bindToVertex(slot);
         }

      template <RenderSystem RS>
      void Texture<RS>::bindToGeometry(uint32 slot) const
         {
         texture->bindToGeometry(slot);
         }

      template <RenderSystem RS>
      void Texture<RS>::bindToFragment(uint32 slot) const
         {
         texture->bindToFragment(slot);
         }

      template <RenderSystem RS>
      void Texture1D<RS>::initCustom(uint32 size, StructureFormat format)
         {
         try
            {
            tex1d.init(size, format);
            texSize = size;
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

      template <RenderSystem RS>
      void Texture1D<RS>::fill(byte* data, uint32 size)
         {
         try
            {
            if(texSize) //indicates successful init
               tex1d.fill(data, size);
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

      template <RenderSystem RS, class T>
      void _Texture2D<RS, T>::initCustom(uint32 width, uint32 height, 
         StructureFormat format)
         {
         try
            {
            tex2d.init(width, height, format);
            texSize = width * height;
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

      template <RenderSystem RS, class T>
      void _Texture2D<RS, T>::fill(byte* data, uint32 size)
         {
         try
            {
            if(texSize) //indicates successful init
               tex2d.fill(data, size);
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

      template <RenderSystem RS>
      void Texture3D<RS>::initCustom(uint32 width, uint32 height, uint32 depth,
         StructureFormat format)
         {
         try
            {
            tex3d.init(width, height, depth, format);
            texSize = width * height * depth;
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

      template <RenderSystem RS>
      void Texture3D<RS>::fill(byte* data, uint32 size)
         {
         try
            {
            if(texSize) //indicates successful init
               tex3d.fill(data, size);
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());
            }
         }

#if (EMP_USE_OPENGL == ON)
      template class Texture<RS_GL43>;
      template class Texture1D<RS_GL43>;
      template class _Texture2D<RS_GL43, APITexture2D<RS_GL43>>;
      template class _Texture2D<RS_GL43, APIRenderTarget<RS_GL43>>;
      template class _Texture2D<RS_GL43, APIDepthStencil<RS_GL43>>;
      template class Texture3D<RS_GL43>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class Texture<RS_DX11>;
      template class Texture1D<RS_DX11>;
      template class _Texture2D<RS_DX11, APITexture2D<RS_DX11>>;
      template class _Texture2D<RS_DX11, APIRenderTarget<RS_DX11>>;
      template class _Texture2D<RS_DX11, APIDepthStencil<RS_DX11>>;
      template class Texture3D<RS_DX11>;
#endif
      }
   }