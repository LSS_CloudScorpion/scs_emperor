#ifndef __EMP_OPEN_GL_HPP__
#define __EMP_OPEN_GL_HPP__

#if EMP_USE_OPENGL
#include "Dependencies\GL\glew.h"
#include "Dependencies\GL\wglew.h"
#include "Dependencies\SOIL\SOIL.h"

#pragma comment (lib, "OpenGL32.lib")
#endif

#endif