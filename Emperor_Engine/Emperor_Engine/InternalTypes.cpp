#define EMP_EXPORT_DLL
#include <Core\EmperorAPI.hpp>
#include "InternalTypes.hpp"

namespace Emperor
   {
   iFile* _defaultCreateFile()
      {
      return new File;
      }

   EMP_API void setCreateFile(iFile*(*a)())
      {
      if(a)
         File::create = a;
      else
         File::create = _defaultCreateFile;
      }

   iFile*(*File::create)() = _defaultCreateFile;
   }