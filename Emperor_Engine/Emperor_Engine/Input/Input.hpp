#ifndef __EMP_INP_INPUT_HPP__
#define __EMP_INP_INPUT_HPP__

#include <Input/iInput.hpp>
#include "InternalTypes.hpp"

namespace Emperor
   {
   namespace Input
      {
      struct _KeyPair
         {
         InputID a;
         InputState b;

         _KeyPair(InputID z,InputState x) : a(z),b(x) {}
         bool operator<(const _KeyPair& c) const
            {
            return (a<<4 | b) < (c.a << 4 | c.b);
            }
         };


      class Input : public iInput
         {
         private:
            HashMap<_KeyPair,ArrayList<InputPkg>> callbacks;
            HashMap<InputID, InputState> lastState;
         protected:
            ~Input() {}
         public:
            Input() {}

            void registerCallback(InputID,InputState,INPUT_CALLBACK_T,void* = 0);
            void unregisterCallback(InputID,InputState,INPUT_CALLBACK_T,void* = 0);

            void destroy();

            void _fireEvent(const DIMOUSESTATE&);
            void _fireEvent(const byte*);
         };
      }
   }
#endif