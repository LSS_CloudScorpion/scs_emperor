#ifndef __EMP_INP_INTERNAL_TYPES_HPP__
#define __EMP_INP_INTERNAL_TYPES_HPP__

#include "../Internals.hpp"
#include <Input/Types.hpp>
#include "../DirectX.hpp"

namespace Emperor
   {
   namespace Input
      {
      typedef Pair<INPUT_CALLBACK_T, void*> InputPkg;
      }
   }

#endif