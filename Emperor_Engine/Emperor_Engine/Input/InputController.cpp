#include "InputController.hpp"

namespace Emperor
   {
   namespace Input
      {
      void InputController::bindWindow(Utility::iWindow* w) 
         { inMan.initialize((Utility::Window*)w); }
      iInput* InputController::getKeyboard() 
         { return inMan.getResource("EMP_KEYBOARD"); }
      iInput* InputController::getMouse() 
         { return inMan.getResource("EMP_MOUSE"); }
      void InputController::fireEvents()
         { inMan.fireEvents(); }


      EMP_API iInputController* getController()
         {
         if(!InputController::getPtr())
            new InputController();
         return InputController::getPtr();
         }
      }
   }