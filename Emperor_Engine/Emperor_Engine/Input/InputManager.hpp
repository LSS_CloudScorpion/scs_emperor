#ifndef __EMP_INP_INPUT_MANAGER_HPP__
#define __EMP_INP_INPUT_MANAGER_HPP__

#include "../ResourceManager.hpp"
#include "Input.hpp"
#include <dinput.h>

namespace Emperor
   {
   namespace Utility
      {
      class Window;
      }
   namespace Input
      {
      class InputManager : public Singleton<InputManager>,public ResourceManager<Input>
         {
         private:
            LPDIRECTINPUT input;
            LPDIRECTINPUTDEVICE keyboard;
            LPDIRECTINPUTDEVICE mouse;

            DIMOUSESTATE md;
            byte kd[256];

            Utility::Window* win;

         protected:
            Input* createResource(const String& r);
         public:
            InputManager();
            virtual ~InputManager();

            void fireEvents();
            void initialize(Utility::Window* w);
         };
      }
   }
#endif