#include "InputManager.hpp"
#include "../Utilities/Window.hpp"

namespace Emperor
   {
   namespace Input
      {

      InputManager::InputManager() : win(0), input(0), keyboard(0), mouse(0)
         {
         ZeroMemory(&md, sizeof(md));
         ZeroMemory(&kd, sizeof(kd));
         }


      void InputManager::initialize(Utility::Window* w)
         {
         //Fill Me
         }

      Input* InputManager::createResource(const String& r)
         {
         //Fill Me
         return resources[r] = new Input();
         }

      InputManager::~InputManager()
         {
         //Fill Me
         }

      void InputManager::fireEvents()
         {
         //Fill Me
         }
      }
   }