#ifndef __EMP_INP_INPUT_CONTROLLER_HPP__
#define __EMP_INP_INPUT_CONTROLLER_HPP__

#include "InputManager.hpp"
#include <Input/iInput.hpp>
#include <Input/iInputController.hpp>
#include "../Utilities/Window.hpp"

namespace Emperor
   {
   namespace Input
      {
      class InputController : public iInputController, 
         public Singleton<InputController, true>
         {
         private:
            InputManager inMan;
         protected:
         public:
            void bindWindow(Utility::iWindow* w);
            void fireEvents();
            iInput* getKeyboard();
            iInput* getMouse();
         };
      }
   }

#endif