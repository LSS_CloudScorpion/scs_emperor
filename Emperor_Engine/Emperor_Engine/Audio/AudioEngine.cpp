#include "Engine.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      void Engine<AS>::initialize()
         {
         if(dev.isActive())
            dev.release();
         dev.init();
         }

      template <AudioSystem AS>
      void Engine<AS>::release()
         {
         dev.release();
         }

      template <AudioSystem AS>
      void Engine<AS>::setResourceFolder(const LString& r)
         {
         resourceFolder = r.stdString();
         if(!resourceFolder.size())
            resourceFolder = "./";
         if(resourceFolder.back() != '\\' && resourceFolder.back() != '/')
            resourceFolder += '/';
         }


      template <AudioSystem AS>
      void Engine<AS>::updateSystem()
         {
         if(dev.isActive() && aCont.lstMan.getCurrent())
            {
            aCont.lstMan.getCurrent()->update();
            aCont.emtMan.updatePlayState();
            aCont.emtMan.prepareLoop();
            while(aCont.emtMan.bindNext())
               {
               aCont.emtMan.getCurrent()->update();
               }
            }
         }



      EMP_API iEngine* createEngine(AudioSystem t, const LString& resourceFolder)
         {
         if(t == AS_DX11)
            {
#if EMP_USE_DIRECTX
            Engine<AS_DX11>* engine = new Engine<AS_DX11>();
            engine->setResourceFolder(resourceFolder);
            return engine;
#else
            return nullptr;
#endif
            }
         else
            {
#if EMP_USE_OPENAL
            Engine<AS_AL11>* engine = new Engine<AS_AL11>();
            engine->setResourceFolder(resourceFolder);
            return engine;
#else
            return nullptr;
#endif
            }
         }

      void releaseEngine(iEngine* a)
         {
         delete a;
         }


      }
   }