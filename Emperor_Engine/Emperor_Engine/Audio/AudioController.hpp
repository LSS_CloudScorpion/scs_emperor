#ifndef __EMP_AUD_AUDIO_CONTROLLER_HPP__
#define __EMP_AUD_AUDIO_CONTROLLER_HPP__

#include <Audio/iAudioController.hpp>
#include "Managers.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      class AudioController : public iAudioController
         {
         private:
         protected:
         public:
            EmitterManager<AS> emtMan;
            ListenerManager<AS> lstMan;
            SoundManager<AS> sndMan;

            virtual const iSound* getSound(const LString& r)
               {return sndMan.getResource(r.stdString());}

            virtual iSound* newCustomSound(const LString& r)
               {return sndMan.newResource(r.stdString());}

            virtual iEmitter* createEmitter()
               {return emtMan.createObject();}
            virtual iListener* createListener()
               {return lstMan.createObject();}
         };
      }
   }

#endif