#ifndef __EMP_AUD_API_LISTENER_HPP__
#define __EMP_AUD_API_LISTENER_HPP__
#include "../OpenAL.hpp"
#include "../DirectX.hpp"
#include <Audio/Types.hpp>

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      class APIListener{};

#if EMP_USE_DIRECTX

      template<>
      class APIListener<AS_DX11>
         {
         private:
            X3DAUDIO_LISTENER lst;
         protected:
         public:
            APIListener() 
               {
               ZeroMemory(&lst, sizeof(lst));
               }

            void update(Matrix<float,4>);
            void setVolume(float);

            const X3DAUDIO_LISTENER& _exposeAPI() {return lst;}

            virtual ~APIListener() {}
         };
#endif

#if EMP_USE_OPENAL

      template<>
      class APIListener<AS_AL11>
         {
         private:
         protected:
         public:
            void update(Matrix<float,4>);
            void setVolume(float);

            virtual ~APIListener() {}
         };


#endif
      }

   }

#endif