#ifndef __EMP_AUD_MANAGERS_HPP__
#define __EMP_AUD_MANAGERS_HPP__

#include "InternalTypes.hpp"
#include "../BaseManager.hpp"
#include "../ResourceManager.hpp"
#include "Emitter.hpp"
#include "Listener.hpp"
#include "Sound.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      class EmitterManager : public Singleton<EmitterManager<AS>>, public IterativeManager<Emitter<AS>>
         {
         private:
         protected:
         public:
            bool bindNext();
            void updatePlayState();
         };
      
      template <AudioSystem AS>
      class ListenerManager : public Singleton<ListenerManager<AS>>, public BaseManager<Listener<AS>>
         {
         private:
            Listener<AS>* active;
         protected:
         public:
            ListenerManager() : active(0) {}

            void activateObject(Listener<AS>* a) {active = a;}
            void deactivateObject(Listener<AS>* a);

            Listener<AS>* getCurrent() {return active;}
         };
      
      template <AudioSystem AS>
      class SoundManager : public Singleton<SoundManager<AS>>, public ResourceManager<Sound<AS>>
         {
         private:
         protected:
            Sound<AS>* createResource(const String&);
         public:
         };
      }
   }

#endif