#include "Managers.hpp"

namespace Emperor
   {
   namespace Audio
      {

      template <AudioSystem AS>
      Sound<AS>* SoundManager<AS>::createResource(const String& r)
         {
         Sound<AS>* retVal = new Sound<AS>();

         retVal->load(r);

         return retVal;
         }

      template <AudioSystem AS>
      void ListenerManager<AS>::deactivateObject(Listener<AS>* a)
         {
         if(a == active)
            active = 0;
         }

      template <AudioSystem AS>
      bool EmitterManager<AS>::bindNext()
         {
         if(it != activeObjects.end())
            {
            it++;
            return true;
            }
         return false;
         }

      template <AudioSystem AS>
      void EmitterManager<AS>::updatePlayState()
         {
         for(auto i = objects.begin(), e = objects.end(); i < e; i++)
            {
            (*i)->isPlaying();//updates the play state
            }
         }

#if (EMP_USE_OPENAL == ON)
      template class SoundManager<AS_AL11>;
      template class EmitterManager<AS_AL11>;
      template class ListenerManager<AS_AL11>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class SoundManager<AS_DX11>;
      template class EmitterManager<AS_DX11>;
      template class ListenerManager<AS_DX11>;
#endif

      }
   }