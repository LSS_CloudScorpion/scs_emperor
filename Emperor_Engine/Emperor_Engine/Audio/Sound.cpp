#include "Sound.hpp"
#include "Engine.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      void Sound<AS>::load(const String& fn) 
         {
         snd.loadFromFile(Engine<AS>::getPtr()->getResourceFolder().stdString()
            + fn);
         }

      template <AudioSystem AS>
      void Sound<AS>::fill(const byte* d, uint32 c) 
         {
         snd.fill(d, c);
         }

      template <AudioSystem AS>
      void Sound<AS>::destroy() 
         {
         SoundManager<AS>::getPtr()->removeResource(this);
         delete this;
         }

#if (EMP_USE_OPENAL == ON)
      template class Sound<AS_AL11>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class Sound<AS_DX11>;
#endif

      }

   }