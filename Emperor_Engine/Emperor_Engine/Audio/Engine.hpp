#ifndef __EMP_AUD_ENGINE_HPP__
#define __EMP_AUD_ENGINE_HPP__

#include <Audio/iEngine.hpp>
#include "Device.hpp"
#include "AudioController.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      class Engine : public iEngine, public Singleton<Engine<AS>, true>
         {
         private:
            EMP_API friend iEngine* createEngine(AudioSystem, const LString&);
            EMP_API friend void releaseEngine(iEngine*);

            String resourceFolder;

            Device<AS> dev;

            AudioController<AS> aCont;
         protected:
            Engine() : resourceFolder("./") {}
         public:
            virtual ~Engine() {}
            void initialize();
            void release();

            void setResourceFolder(const LString&);
            LString getResourceFolder() const {return resourceFolder;}

            void updateSystem();

            void updateSound(APIEmitter<AS>* a)
               {
               auto lst = aCont.lstMan.getCurrent();
               if(lst)
                  dev.updateSound(a, &lst->_exposeAPI());
               }

            iAudioController* getAudioController() {return &aCont;}
            Device<AS>& _getPlatformDevice() {return dev;}
         };

      }

   }

#endif