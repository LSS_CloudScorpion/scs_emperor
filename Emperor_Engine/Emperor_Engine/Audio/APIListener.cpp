#include "APIListener.hpp"
#include "Engine.hpp"

namespace Emperor
   {
   namespace Audio
      {
#if EMP_USE_DIRECTX
      void APIListener<AS_DX11>::update(Matrix<float,4> t)
         {
         //Fill Me
         }

      void APIListener<AS_DX11>::setVolume(float v)
         {
         //Fill Me
         }

#endif

#if EMP_USE_OPENAL
      void APIListener<AS_AL11>::update(Matrix<float,4> t)
         {
         //Fill Me
         }

      void APIListener<AS_AL11>::setVolume(float v)
         {
         //Fill Me
         }

#endif
      }
   }