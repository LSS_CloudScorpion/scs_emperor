#ifndef __EMP_AUD_LISTENER_HPP__
#define __EMP_AUD_LISTENER_HPP__

#include <Audio/iListener.hpp>
#include "../SceneObject.hpp"
#include "APIListener.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      class Listener : public SceneObject<iListener>
         {
         private:
            APIListener<AS> lst;

            float vol;
         protected:
            virtual ~Listener() {}
         public:
            Listener() : vol(1) {}
            void setVolume(float v) {lst.setVolume(vol = v);} 
            float getVolume() {return vol;}

            void activate();
            void deactivate();
            void update();

            void destroy();

            APIListener<AS>& _exposeAPI() {return lst;}
         };
      }

   }

#endif