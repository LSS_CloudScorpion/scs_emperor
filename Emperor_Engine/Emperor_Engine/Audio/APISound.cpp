#include "APISound.hpp"
#include "Engine.hpp"
#include <fstream>

namespace Emperor
   {
   namespace Audio
      {
#if EMP_USE_DIRECTX
      APISound<AS_DX11>::~APISound()
         {
         if(data)
            delete [] data;
         }

      // Source of code from
      // http://msdn.microsoft.com/en-us/library/windows/desktop/ee415781%28v=vs.85%29.aspx

      HRESULT APISound<AS_DX11>::FindChunk(HANDLE hFile, uint32 fourcc, uint32& dwChunkSize, uint32& dwChunkDataPosition)
         {
         HRESULT hr = S_OK;
         if( INVALID_SET_FILE_POINTER == SetFilePointer( hFile, 0, NULL, FILE_BEGIN ) )
            return HRESULT_FROM_WIN32( GetLastError() );

         DWORD dwChunkType;
         DWORD dwChunkDataSize;
         DWORD dwRIFFDataSize = 0;
         DWORD dwFileType;
         DWORD bytesRead = 0;
         DWORD dwOffset = 0;

         while (hr == S_OK)
            {
            DWORD dwRead;
            if( 0 == ReadFile( hFile, &dwChunkType, sizeof(DWORD), &dwRead, NULL ) )
               hr = HRESULT_FROM_WIN32( GetLastError() );

            if( 0 == ReadFile( hFile, &dwChunkDataSize, sizeof(DWORD), &dwRead, NULL ) )
               hr = HRESULT_FROM_WIN32( GetLastError() );

            switch (dwChunkType)
               {
               case 'FFIR':
                  dwRIFFDataSize = dwChunkDataSize;
                  dwChunkDataSize = 4;
                  if( 0 == ReadFile( hFile, &dwFileType, sizeof(DWORD), &dwRead, NULL ) )
                     hr = HRESULT_FROM_WIN32( GetLastError() );
                  break;

               default:
                  if( INVALID_SET_FILE_POINTER == SetFilePointer( hFile, dwChunkDataSize, NULL, FILE_CURRENT ) )
                     return HRESULT_FROM_WIN32( GetLastError() );            
               }

            dwOffset += sizeof(DWORD) * 2;

            if (dwChunkType == fourcc)
               {
               dwChunkSize = dwChunkDataSize;
               dwChunkDataPosition = dwOffset;
               return S_OK;
               }

            dwOffset += dwChunkDataSize;

            if (bytesRead >= dwRIFFDataSize) return S_FALSE;

            }

         return S_OK;
         }

      HRESULT APISound<AS_DX11>::ReadChunkData(HANDLE hFile, void * buffer, uint32 buffersize, uint32 bufferoffset)
         {
         HRESULT hr = S_OK;
         if( INVALID_SET_FILE_POINTER == SetFilePointer( hFile, bufferoffset, NULL, FILE_BEGIN ) )
            return HRESULT_FROM_WIN32( GetLastError() );
         DWORD dwRead;
         if( 0 == ReadFile( hFile, buffer, buffersize, &dwRead, NULL ) )
            hr = HRESULT_FROM_WIN32( GetLastError() );
         return hr;
         }

      void APISound<AS_DX11>::loadFromFile(const String& file)
         {
         ZeroMemory(&wfx, sizeof(wfx));
         if(data)
            delete [] data;
         cnt = 0;

         // Open the file
         HANDLE hFile = CreateFile(
            file.c_str(),
            GENERIC_READ,
            FILE_SHARE_READ,
            NULL,
            OPEN_EXISTING,
            0,
            NULL );

         if( INVALID_HANDLE_VALUE == hFile ||  INVALID_SET_FILE_POINTER == SetFilePointer( hFile, 0, NULL, FILE_BEGIN ) )
            EMP_RESOURCE_ERROR(file + " failed to load file");



         uint32 dwChunkPosition;
         //check the file type, should be fourccWAVE or 'XWMA'
         FindChunk(hFile,'FFIR',cnt, dwChunkPosition );
         DWORD filetype;
         ReadChunkData(hFile,&filetype,sizeof(DWORD),dwChunkPosition);
         if (filetype != 'EVAW')
            EMP_RESOURCE_ERROR(file + " failed to load file");


         FindChunk(hFile,' tmf', cnt, dwChunkPosition );
         ReadChunkData(hFile, &wfx, cnt, dwChunkPosition );

         //fill out the audio data buffer with the contents of the fourccDATA chunk
         FindChunk(hFile,'atad',cnt, dwChunkPosition );
         data = new BYTE[cnt];
         ReadChunkData(hFile, data, cnt, dwChunkPosition);
         }

      void APISound<AS_DX11>::fill(const byte* d, uint32 count)
         {
         if(data)
            delete [] data;

         data = new byte[count];
         memcpy(data, d, count);
         cnt = count;
         }
#endif

#if EMP_USE_OPENAL
      APISound<AS_AL11>::~APISound()
         {
         if(data)
            delete [] data;
         }

      void APISound<AS_AL11>::loadFromFile(const String& file)
         {
         std::fstream fs(file.c_str(), std::ios::in | std::ios::binary);
         
         if(!fs)
            EMP_RESOURCE_ERROR(String("Unable to load ") + file);

         char type[4];
         uint32 size, chunksize, avgBytesPerSec;
         short formatType, channels, bytesPerSample, bitsPerSample;

         fs.read(type, sizeof(type));

         if(*(int*)type != 'FFIR')
            EMP_RESOURCE_ERROR(file + " file not compatable sound format");

         fs.read((char*)&size, sizeof(size));
         fs.read(type, sizeof(type));

         if(*(int*)type != 'EVAW')
            EMP_RESOURCE_ERROR(file + " file not compatable sound format");

         fs.read(type, sizeof(type));

         if(*(int*)type != ' tmf')
            EMP_RESOURCE_ERROR(file + " file not compatable sound format");

         fs.read((char*)&chunksize, sizeof(chunksize));
         fs.read((char*)&formatType, sizeof(formatType));
         fs.read((char*)&channels, sizeof(channels));
         fs.read((char*)&freq, sizeof(freq));
         fs.read((char*)&avgBytesPerSec, sizeof(avgBytesPerSec));
         fs.read((char*)&bytesPerSample, sizeof(bytesPerSample));
         fs.read((char*)&bitsPerSample, sizeof(bitsPerSample));

         fs.read(type, sizeof(type));

         if(*(int*)type != 'atad')
            EMP_RESOURCE_ERROR(file + " file not compatable sound format");

         fs.read((char*)&cnt, sizeof(cnt));

         data = new byte[cnt];
         fs.read((char*)data, cnt);

         wfx = bitsPerSample == 8 ? channels == 1 ? AL_FORMAT_MONO8 : AL_FORMAT_STEREO8
            : channels == 1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;
         }

      void APISound<AS_AL11>::fill(const byte* d, uint32 count)
         {
         if(data)
            delete [] data;

         data = new byte[count];
         memcpy(data, d, count);
         cnt = count;
         }
#endif
      }
   }