#ifndef __EMP_AUD_SOUND_HPP__
#define __EMP_AUD_SOUND_HPP__

#include <Audio/iSound.hpp>
#include "APISound.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      class Sound : public iSound
         {
         private:
            APISound<AS> snd;
         protected:
            virtual ~Sound() {}
         public:
            void load(const String&);
            void fill(const byte*, uint32);
            void destroy();

            APISound<AS>& _exposeAPI() {return snd;}
         };
      }
   }

#endif