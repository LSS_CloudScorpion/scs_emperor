#ifndef __EMP_AUD_API_EMITTER_HPP__
#define __EMP_AUD_API_EMITTER_HPP__
#include "../OpenAL.hpp"
#include "../DirectX.hpp"
#include <Audio/Types.hpp>

namespace Emperor
   {
   namespace Audio
      {

      template <AudioSystem AS>
      class APIEmitter{};

#if EMP_USE_DIRECTX
      template<>
      class APIEmitter<AS_DX11>
         {
         private:
            IXAudio2SourceVoice* snd;
            XAUDIO2_BUFFER buffer;
            WAVEFORMATEXTENSIBLE format;
            X3DAUDIO_EMITTER emit;
         protected:
         public:
            APIEmitter() : snd(0)
               {
               ZeroMemory(&buffer, sizeof(buffer));
               ZeroMemory(&format, sizeof(format));
               ZeroMemory(&emit, sizeof(emit));
               }

            virtual ~APIEmitter();

            void init();

            void updateBuffer(const byte*, uint32, uint32);
            void _setFormat(const WAVEFORMATEXTENSIBLE& f) {format = f;}
            void bindSound();
            void start();
            void stop();
            void pause();
            void unpause();
            bool isPlaying();

            void update(Matrix<float,4>);

            void setVolume(float);
            void setRepeat(bool);

            void resetState();

            IXAudio2SourceVoice* _exposeAPI() {return snd;}
            X3DAUDIO_EMITTER& _exposeEmitter() {return emit;}
         };
#endif

#if EMP_USE_OPENAL
      template<>
      class APIEmitter<AS_AL11>
         {
         private:
            ALuint src;
            ALuint buffer;
            ALenum format;
         protected:
         public:
            APIEmitter() : src(0), buffer(0), format(0) {}

            virtual ~APIEmitter();

            void init();

            void updateBuffer(const byte*, uint32, uint32);
            void _setFormat(const ALenum f) {format = f;}
            void bindSound();
            void start();
            void stop();
            void pause();
            void unpause();
            bool isPlaying();

            void update(Matrix<float,4>);

            void setVolume(float);
            void setRepeat(bool);

            void resetState();

            ALuint _exposeAPI() {return src;}
         };
#endif
      }
   }

#endif