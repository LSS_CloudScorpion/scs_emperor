#include "APIEmitter.hpp"
#include "Engine.hpp"

namespace Emperor
   {
   namespace Audio
      {
#if EMP_USE_DIRECTX

      void APIEmitter<AS_DX11>::init()
         {
         //Fill Me
         }

      void APIEmitter<AS_DX11>::resetState() 
         {
         //Fill Me
         }


      APIEmitter<AS_DX11>::~APIEmitter()
         {
         //Fill Me
         }

      void APIEmitter<AS_DX11>::updateBuffer(const byte* d, uint32 c, uint32)
         {
         //Fill Me
         }

      void APIEmitter<AS_DX11>::bindSound()
         {
         //Fill Me
         }

      void APIEmitter<AS_DX11>::start()
         {
         //Fill Me
         }

      void APIEmitter<AS_DX11>::setVolume(float v)
         {
         //Fill Me
         }

      void APIEmitter<AS_DX11>::stop()
         {
         //Fill Me
         }

      void APIEmitter<AS_DX11>::pause()
         {
         //Fill Me
         }

      void APIEmitter<AS_DX11>::unpause()
         {
         //Fill Me
         }

      bool APIEmitter<AS_DX11>::isPlaying()
         {
         //Fill Me
         return false;
         }

      void APIEmitter<AS_DX11>::update(Matrix<float,4> t)
         {
         //Fill Me
         }

      void APIEmitter<AS_DX11>::setRepeat(bool r)
         {
         //Fill Me
         }
#endif
#if EMP_USE_OPENAL

      void APIEmitter<AS_AL11>::init()
         {
         //Fill Me
         }

      void APIEmitter<AS_AL11>::resetState() 
         {
         }


      APIEmitter<AS_AL11>::~APIEmitter()
         {
         //Fill Me
         }

      void APIEmitter<AS_AL11>::updateBuffer(const byte* d, uint32 c, uint32 f)
         {
         //Fill Me
         }

      void APIEmitter<AS_AL11>::bindSound()
         {
         //not needed
         }

      void APIEmitter<AS_AL11>::start()
         {
         //Fill Me
         }

      void APIEmitter<AS_AL11>::setVolume(float v)
         {
         //Fill Me
         }

      void APIEmitter<AS_AL11>::stop()
         {
         //Fill Me
         }

      void APIEmitter<AS_AL11>::pause()
         {
         //Fill Me
         }

      void APIEmitter<AS_AL11>::unpause()
         {
         //Fill Me
         }

      bool APIEmitter<AS_AL11>::isPlaying()
         {
         //Fill Me
         return false;
         }

      void APIEmitter<AS_AL11>::update(Matrix<float,4> t)
         {
         //Fill Me
         }

      void APIEmitter<AS_AL11>::setRepeat(bool r)
         {
         //Fill Me
         }
#endif

      }
   }