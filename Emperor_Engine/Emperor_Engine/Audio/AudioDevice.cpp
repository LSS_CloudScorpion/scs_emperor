#include "Device.hpp"
#include "APIEmitter.hpp"
#include "APIListener.hpp"
#include "InternalTypes.hpp"
#include "../Errors.hpp"

namespace Emperor
   {
   namespace Audio
      {
#if EMP_USE_DIRECTX
      Device<AS_DX11>::Device() : engine(0), master(0)
         {
         CoInitializeEx(NULL, COINIT_MULTITHREADED);
         ZeroMemory(&set, sizeof(set));
         }

      Device<AS_DX11>::~Device()
         {
         release();
         CoUninitialize();
         }

      void Device<AS_DX11>::init()
         {
         //Fill Me
         }


      void Device<AS_DX11>::updateSound(APIEmitter<AS_DX11>* a, APIListener<AS_DX11>* b)
         {
         //Fill Me
         }

      void Device<AS_DX11>::release()
         {
         //Fill Me
         }
#endif

#if EMP_USE_OPENAL

      Device<AS_AL11>::~Device()
         {
         release();
         }

      void Device<AS_AL11>::init()
         {
         //Fill Me
         }


      void Device<AS_AL11>::updateSound(APIEmitter<AS_AL11>* a, APIListener<AS_AL11>* b)
         {
         }

      void Device<AS_AL11>::release()
         {
         //Fill Me
         }
#endif
      }
   }