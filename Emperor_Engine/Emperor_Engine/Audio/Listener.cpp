#include "Listener.hpp"
#include "Managers.hpp"

namespace Emperor
   {
   namespace Audio
      {

      template <AudioSystem AS>
      void Listener<AS>::update() 
         {
         lst.update(node->_getAbs());
         }

      template <AudioSystem AS>
      void Listener<AS>::activate() 
         {
         if(node)
            ListenerManager<AS>::getPtr()->activateObject(this);
         }


      template <AudioSystem AS>
      void Listener<AS>::deactivate() 
         {
         ListenerManager<AS>::getPtr()->deactivateObject(this);
         }

      template <AudioSystem AS>
      void Listener<AS>::destroy() 
         {
         deactivate();
         delete this;
         }




#if (EMP_USE_OPENAL == ON)
      template class Listener<AS_AL11>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class Listener<AS_DX11>;
#endif

      }

   }