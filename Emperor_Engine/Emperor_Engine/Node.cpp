#include "Node.hpp"
#include "SceneObject.hpp"
#include "NodeManager.hpp"

namespace Emperor
   {
   void Node::setPosition(const Vector<float, 3>& pos)
      {
      Emperor::setPosition(localT, pos);
      }

   Vector<float, 3> Node::getPosition() const
      {
      return Emperor::getPosition(localT);
      }

   void Node::translate(const Vector<float, 3>& p)
      {
      Emperor::translate(localT, p);
      }

   void Node::setScale(const Vector<float, 3>& s)
      {
      Emperor::scale(Vector3(1 / localScale.x(), 1 / localScale.y(),
         1 / localScale.z()), localT);
      localScale = s;
      Emperor::scale(s, localT);
      }

   void Node::scale(const Vector<float, 3>& s)
      {
      localScale = Vector3(localScale.x() * s.x(), localScale.y() * s.y(),
         localScale.z() * s.z());
      Emperor::scale(s, localT);
      }

   void Node::setRotation(const Matrix<float, 3>& r)
      {
      Matrix<float, 3> tr;
      tr[0][0] = localScale.x();
      tr[1][1] = localScale.y();
      tr[2][2] = localScale.z();
      tr[0][1] = tr[0][2] = tr[1][0] = tr[1][2] = tr[2][0] = tr[2][1] = 0;

      tr = tr * r;

      localT = Matrix4(Vector4(tr[0], 0), Vector4(tr[1], 0), Vector4(tr[2], 0),
         Vector4(getPosition(), 1));
      }

   Matrix<float, 3> Node::getRotation() const
      {
      Matrix<float, 3> t;
      for(int i = 0; i < 3; i++)
         {
         for(int j = 0; j < 3; j++)
            t[i][j] = localT[i][j];
         }
      Matrix<float, 3> tr;
      tr[0][0] = 1 / localScale.x();
      tr[1][1] = 1 / localScale.y();
      tr[2][2] = 1 / localScale.z();
      tr[0][1] = tr[0][2] = tr[1][0] = tr[1][2] = tr[2][0] = tr[2][1] = 0;

      return tr * t;
      }

   void Node::rotate(const Vector<float, 3>& axis, float radians)
      {
      Emperor::rotate(localT, axis, radians);
      }

   void Node::attachTo(iNode* p)
      {
      //If (p) is not null and current node has a parent, remove the node
      //from the parent
      //If (p) is not null and current node doesn't have a parent, deactivate
      //the node
      //If (p) is not null, add the current node to (p)'s child list
      //If (p) is null and the current node has a parent, remove the node
      //from the parent and active the node
      //set the current node's parent to (p)
      }

   void Node::_removeChild(Node* a)
      {
      //Remove (a) from the child vector
      }

   void Node::_updateAbs(const Matrix<float, 4>& a)
      {
      //Set the absolute transform of the current node and then recursively
      //call the update function on all the children
      }

   void Node::_recalcAbsTransform()
      {
      _tempAbs = localT;
      auto* p = parent;
      while(p)
         {
         _tempAbs = _tempAbs * p->localT;
         p = p->parent;
         }
      }

   const Matrix<float, 4>& Node::_getAbsTransform() const
      {
      return _tempAbs;
      }

   void Node::destroy()
      {
      const auto t = children;
      for(auto i = t.begin(), end = t.end(); i < end; i++)
         (*i)->attachTo(0);
      const auto o = objects;
      for(auto i = o.begin(), end = o.end(); i < end; i++)
         (*i)->attachTo(0);
      if(parent)
         parent->_removeChild(this);
      NodeManager::getPtr()->deactivateObject(this);
      NodeManager::getPtr()->removeObject(this);
      delete this;
      }

   void Node::_notifyDetach(SceneObject<>* a)
      {
      auto i = searchList(objects.begin(), objects.end(), a);
      if(i != objects.end())
         objects.erase(i);
      }
   }