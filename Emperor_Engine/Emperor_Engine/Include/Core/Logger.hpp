#ifndef __EMP_LOGGER_HPP__
#define __EMP_LOGGER_HPP__

#include <Core/EmperorAPI.hpp>

namespace Emperor
   {
   EMP_API void setLogger(void(*)(const char*));
   }

#endif