#ifndef __EMP_TYPES_HPP__
#define __EMP_TYPES_HPP__

#include <Core\EmperorAPI.hpp>
#include <cstdint>
#include <string>
#include <vector>
#include "Math.hpp"

namespace Emperor
   {
   //Integer typedefs
   typedef uint64_t uint64;
   typedef uint32_t uint32;
   typedef uint16_t uint16;
   typedef uint8_t uint8;
   typedef uint8 byte;

   typedef void* WindowID;

   //Light weight array class, simply for exporting
   template <class T>
   class LArray
      {
      private:
         T* data;
         uint32 curSize;
         uint32 maxSize;

         void resize(uint32 s)
            {
            T* t = new T[s];
            for(uint32 i = 0; i < curSize; i++)
               t[i] = data[i];
            delete[] data;
            data = t;
            maxSize = s;
            }
      protected:
      public:
         LArray(uint32 s = 8, T* d = nullptr)
            {
            data = new T[0];
            set(s, d);
            }

         LArray(const std::vector<T>& v)
            {
            data = new T[0];
            curSize = maxSize = 0;
            if(v.size())
               set(v.size(), &v.front());
            }

         LArray(const LArray& d)
            {
            data = new T[0];
            set(d.curSize, d.data);
            }

         ~LArray()
            {
            delete[] data;
            }

         LArray& operator=(const LArray& d)
            {
            if(this != &d)
               {
               set(d.curSize, d.data);
               }
            return *this;
            }

         void set(uint32 s, const T* d = nullptr)
            {
            delete[] data;
            data = new T[s];
            maxSize = s;
            curSize = 0;
            if(d)
               {
               for(; curSize < s; curSize++)
                  data[curSize] = d[curSize];
               }
            }

         void add(const T& d)
            {
            if(curSize == maxSize)
               resize(maxSize ? maxSize * 2 : 1);
            data[curSize++] = d;
            }

         void add(const T* d)
            {
            if(d)
               add(*d);
            }

         void push_back(const T& d)
            {
            add(d);
            }

         T& operator[](uint32 i)
            {
            return data[i];
            }

         const T& operator[](uint32 i) const
            {
            return data[i];
            }

         T& front()
            {
            return data[0];
            }

         const T& front() const
            {
            return data[0];
            }

         uint32 size() const
            {
            return curSize;
            }

         std::vector<T> stdVector() const
            {
            return std::vector<T>(data, data + curSize);
            }
      };

   class LString : public LArray<char>
      {
      private:
      protected:
      public:
         LString()
            {
            set(1, "");
            }

         LString(const char* str)
            {
            set(strlen(str) + 1, str);
            }

         LString(const std::string& str)
            {
            set(str.size() + 1, str.c_str());
            }

         std::string stdString() const
            {
            return std::string(&front());
            }
      };

   class iFile
      {
      public:
         virtual void open(const char* filename, std::ios::openmode, bool =true) = 0;
         void open(const LString& f, std::ios::openmode i, bool a = true) 
            { open(&f.front(), i, a); }
         virtual ~iFile() {}

         virtual bool isLoaded() = 0;
         virtual uint32 size() = 0;
         virtual bool isError() = 0;
         virtual void read(byte*, uint32) = 0;
         virtual bool good() = 0;
         virtual char get() = 0;
      };

   EMP_API void setCreateFile(iFile*(*)());
   }
#endif