#ifndef __EMP_INP_I_INPUT_HPP__
#define __EMP_INP_I_INPUT_HPP__

//Basic Input Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Input
      {
      /** Input Interface

      The input class represents a single input device. Once created, input
      devices can have callbacks registered or unregistered and will fire them
      when the device is ticked by the manager.
      */
      class iInput // to iInputDevice
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iInput() {}
            virtual ~iInput() {}
         public:
            //Registers a callback with the device. Parameters indicate the 
            //event that will fire the callback, the state of the event (down, 
            //up, none), the callback function itself and an address to memory
            //that holds data for the call back to manipulate. The callback's 
            //parameters it will receive are the device that called the
            //function, the system time when the event fired, a hardware level
            //device pointer (needing to read from mouse), and the address
            //provided to the register function.
            virtual void registerCallback(InputID id, InputState state, 
               void(*callback)
               (iInput* device, uint64 time, void* hwDevice, void* data), 
               void* data = 0) = 0;

            //Removes a callback from the registry. The system searches for
            //an identical entry and removes it if found, does nothing
            //otherwise.
            virtual void unregisterCallback(InputID id, InputState state, 
               void(*)(iInput*, uint64, void*, void*), 
               void* data = 0) = 0;

            //Cleanly destroys the input device, removing references from 
            //managers and deallocating resources.
            virtual void destroy() = 0;
         };
      }
   }
#endif