#ifndef __EMP_AUD_I_LISTENER_HPP__
#define __EMP_AUD_I_LISTENER_HPP__

//Inherits from the core's scene object
#include "../Core/iSceneObject.hpp"

namespace Emperor
   {
   namespace Audio
      {
      /** Listener Interface

      A listener acts as a point and orientation in space from which to hear
      emitters from. Listeners must be bound to a node to become a part of the
      scene. Only one listener can be active at a single time. 3D audio effects
      required the listener to calculate how distance and orientation affect 
      how the sound is processed from the emitter to the listener.
      */
      class iListener : public iSceneObject
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iListener() {}
            virtual ~iListener() {}
         public:
            //Sets the volume of the system, all emitter's volumes will be
            //effected by the volume of the listener
            virtual void setVolume(float) = 0;

            //Returns the volume of the system
            virtual float getVolume() = 0;
         };
      }

   }

#endif