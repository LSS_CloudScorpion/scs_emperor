#ifndef __EMP_AUD_I_EMITTER_HPP__
#define __EMP_AUD_I_EMITTER_HPP__

//Basic Audio Types
#include "Types.hpp"

//Inherits from the core's scene object
#include "../Core/iSceneObject.hpp"

namespace Emperor
   {
   namespace Audio
      {
      class iSound;

      /** Emitter Interface

      An emitter controls an instance of a sound resource. Emitters read sound
      resources and can control them through playing, pausing, stopping and
      adding 3D effects. For 3D effects, the node will need to be attached
      to scene node.
      */
      class iEmitter : public iSceneObject
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iEmitter() {}
            virtual ~iEmitter() {}
         public:
            //Binds the emitter to a sound resource. This resource defines what
            //the emitter will be controlling and playing. If a name is 
            //provided, it will attempt to retreive it from the sound manager,
            //if not found, it will attempt to load from file.
            virtual void setSound(const LString& name) = 0;
            virtual void setSound(const iSound* sound) = 0;

            //Returns the sound resource bound to the emitter.
            virtual const iSound* getSound() = 0;

            //Plays the sound, in 3D if attached to a node, background
            //otherwise
            virtual void play() = 0;

            //Plays the sound as a background/ambient sound equally into
            //all audio channels
            virtual void play2D() = 0;

            //Plays the sound as a 3D sound. Will need to be attached to a node
            //to function.
            virtual void play3D() = 0;

            //Returns true if the emitter is currently playing a sound
            virtual bool isPlaying() = 0;

            //Pauses a playing sound
            virtual void pause() = 0;

            //Unpauses a playing sound
            virtual void unpause() = 0;

            //Returns true if the emitter is paused
            virtual bool isPaused() = 0;

            //Stops the sound from playing, causing the emitter to start at the
            //beginning of the resource next time it is played
            virtual void stop() = 0;

            //Sets whether or not the sound should repeat
            virtual void setRepeat(bool) = 0;

            //Returns true if the sound is set to repeat
            virtual bool isRepeat() = 0;

            //Sets the volume of the emitter
            virtual void setVolume(float) = 0;

            //Returns the volume of the emitter
            virtual float getVolume() = 0;
         };
      }

   }

#endif