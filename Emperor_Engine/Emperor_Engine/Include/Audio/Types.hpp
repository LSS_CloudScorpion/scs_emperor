#ifndef __EMP_AUD_TYPES_HPP__
#define __EMP_AUD_TYPES_HPP__

#include "../Core/Types.hpp"

namespace Emperor
   {
   namespace Audio
      {
      enum AudioSystem
         {
         AS_DX11,
         AS_AL11
         };
      }
   }
#endif