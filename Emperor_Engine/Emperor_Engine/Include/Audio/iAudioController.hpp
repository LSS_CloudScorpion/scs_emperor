#ifndef __EMP_AUD_I_AUDIO_CONTROLLER_HPP__
#define __EMP_AUD_I_AUDIO_CONTROLLER_HPP__

//Basic Graphic Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Audio
      {
      class iEmitter;
      class iListener;
      class iSound;

      /** Audio Controller Interface

      This interface exposes the high level controls of the audio system.
      The controller provides all required functionality for scene objects and
      resources for the audio engine.
      */
      class iAudioController
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iAudioController() {}
            virtual ~iAudioController() {}
         public:
            //Returns a sound from the sound resource manager. If not found
            //it attempts to load the sound from file using the name as the
            //file name
            virtual const iSound* getSound(const LString& name) = 0;

            //Creates and returns a new empty sound resource. This will need
            //to be filled before being used
            virtual iSound* newCustomSound(const LString& name) = 0;

            //Creats a Emitter scene object. Emitters play sounds which they 
            //are bound to and can play them as background sounds or in 3D
            //space. To play in 3D, the emitter must first be attached to a 
            //node in the scene
            virtual iEmitter* createEmitter() = 0;

            //Creates a Listener scene object. Listeners are used to define how
            //a 3D emitter should sound to the listener by comparing the 
            //spatial information of the node it is attached to to the node
            //of the emitter. There can only be one active listener at any time
            virtual iListener* createListener() = 0;
         };
      }
   }

#endif