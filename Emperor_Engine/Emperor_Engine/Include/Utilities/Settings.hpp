#ifndef __EMP_UTL_SETTINGS_HPP__
#define __EMP_UTL_SETTINGS_HPP__

#include "../Graphics/Types.hpp"
#include "../Audio/Types.hpp"
#include "Types.hpp"
#include <fstream>
namespace Emperor
   {
   namespace Utility
      {
      struct Settings
         {
         static Emperor::byte getValidMultisampleCount(uint32 count);

         Vector<int, 2> resolution;
         bool fullscreen;
         Graphics::RenderSystem renderSystem;
         Audio::AudioSystem audioSystem;
         LString resourceFolder;
         Emperor::byte multisampleCount;
         uint32 vsync;

         //backed in defaults
         Settings() :
            resolution(Coord(800, 600)),
            fullscreen(false),
            renderSystem(Graphics::RS_DX11),
            resourceFolder("./resources"),
            multisampleCount(1),
            audioSystem(Audio::AS_DX11),
            vsync(0)
            {}
         ~Settings() {}

         bool loadFromFile(const LString& filename);
         bool saveToFile(const LString& filename);
         };
      inline Emperor::byte Settings::getValidMultisampleCount(uint32 count_)
         {
         if(count_ > 32) count_ = 32;
         if(count_ < 1) count_ = 1;

         Emperor::byte count = 1 << 7;
         while(count > 1 && !(count & count_))
            count >>= 1;
         return count;
         }

      static void _parseFile(const LArray<LString>& cl, Settings& s)
         {
         auto c = cl.stdVector();
         for(auto i = c.begin(); i < c.end(); ++i)
            {
            auto res = SplitString(i->stdString(), '=');
            if(res.size() < 2) continue; //skip malformed
            if(res[0] == "resolution")
               {
               auto r = SplitString(res[1], 'x');
               if(r.size() < 2) continue; //skip malformed
               int x = atoi(r[0].c_str());
               int y = atoi(r[1].c_str());
               if(x & y)
                  s.resolution = Coord(x, y);
               }
            else if(res[0] == "fullscreen")
               {
               if(res[1] == "true")
                  s.fullscreen = true;
               else if(res[1] == "false")
                  s.fullscreen = false;
               }
            else if(res[0] == "vsync")
               {
               s.vsync = atoi(res[1].c_str());
               }
            else if(res[0] == "rendersystem")
               {
               if(res[1] == "dx11")
                  s.renderSystem = Graphics::RS_DX11;
               else if(res[1] == "gl43")
                  s.renderSystem = Graphics::RS_GL43;
               }
            else if(res[0] == "audiosystem")
               {
               if(res[1] == "dx11")
                  s.audioSystem = Audio::AS_DX11;
               else if(res[1] == "al11")
                  s.audioSystem = Audio::AS_AL11;
               }
            else if(res[0] == "resourcefolder")
               {
               auto resourceFolder = res[1];
               if(!resourceFolder.size())
                  resourceFolder = "./";
               if(resourceFolder.back() != '\\' && resourceFolder.back() != '/')
                  resourceFolder += '/';
               s.resourceFolder = resourceFolder;
               }
            else if(res[0] == "multisamplecount")
               {
               s.multisampleCount =
                  Settings::getValidMultisampleCount(atoi(res[1].c_str()));
               }
            }
         }

      inline bool Settings::loadFromFile(const LString& filename)
         {
         std::fstream f;
         std::vector<LString> contents;
         std::string a;

         f.open(&filename.front());

         if(f.is_open())
            {
            while(f.good())
               {
               std::getline(f, a);
               contents.push_back(a);
               }
            f.close();

            _parseFile(contents, *this);
            return true;
            }

         return false;
         }

      inline bool Settings::saveToFile(const LString& filename)
         {
         std::fstream f;

         f.open(&filename.front(), std::ios::out | std::ios::trunc);

         if(f.is_open())
            {
            f << "resolution=" << resolution.x() << "x" << resolution.y() << "\n";
            f << "fullscreen=" << std::boolalpha << fullscreen << "\n";
            f << "rendersystem=";
            if(renderSystem == Graphics::RS_DX11)
               f << "dx11";
            else if(renderSystem == Graphics::RS_GL43)
               f << "gl43";
            f << "\n";
            f << "audiosystem=";
            if(audioSystem == Audio::AS_DX11)
               f << "dx11";
            else if(audioSystem == Audio::AS_AL11)
               f << "al11";
            f << "\n";
            f << "resourcefolder=" << &resourceFolder.front() << "\n";
            f << "multisamplecount=" << (uint32)multisampleCount << "\n";
            f << "vsync=" << vsync << "\n";

            f.close();
            return true;
            }

         return false;
         }
      }
   }

#endif