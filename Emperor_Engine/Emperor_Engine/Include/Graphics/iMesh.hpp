#ifndef __EMP_GFX_I_MESH_HPP__
#define __EMP_GFX_I_MESH_HPP__

//Basic Graphic Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      class iVertexFormat;

      /** Mesh Interface

      A mesh resource. A mesh is made up of several vertices that collect to
      makes some kind of 3D shape. Meshes are required by the Actor to become 
      a renderable object.
      */
      class iMesh
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iMesh() {}
            virtual ~iMesh() {}
         public:
            //Creates the mesh from the parameters provided. The vertex
            //definition defines the data structure and bindings of the 
            //vertices. VertexList is the list of vertices as a simple
            //byte array. IndexList is the list of indices as unsigned ints.
            //PrimitiveTopology identifies how the vertices will be grouped
            //when forming the mesh.
            virtual void loadFromList(const iVertexFormat& vertexDefinition,
               const LArray<const byte*>& vertexList, 
               const LArray<uint32>& indexList,
               const PrimitiveTopology& primitiveTopology) = 0;

            //Loads a mesh from file. Currently only supports .obj files.
            virtual void loadFromFile(const LString& fileName) = 0;

            //Returns the number of vertices in the mesh.
            virtual uint32 getVertexCount() const = 0;

            //Returns the number of indices in the mesh.
            virtual uint32 getIndexCount() const = 0;

            //Cleanly destroys the object, removing references in managers and
            //deallocating resources.
            virtual void destroy() = 0;
         };
      }
   }

#endif