#ifndef __EMP_GFX_I_MATERIAL_HPP__
#define __EMP_GFX_I_MATERIAL_HPP__

//Basic Graphic Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      /** Material Interface

      A material that can be applied to an actor in the scene. The material
      defines how the actors mesh will be rendered in the scene. A material
      is made up of zero or more techniques (which are made up of zero or more
      passes). A material pass will be associated with all the information 
      required to render the actor, including textures, shaders, uniform data,
      etc. The material also has its own uniform data component to be able to
      provide material specific data to the graphics card.
      */
      class iMaterial
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iMaterial() {}
            virtual ~iMaterial() {}
         public:
            //Increases the size of the uniform buffer in the material, use 
            //this to reconstruct classes and structures from C++ to the 
            //shader. It returns the position in the buffer from which the new
            //space can be identified 
            virtual uint32 addSize(uint32 typeSize) = 0;

            //Sets the value for one of the uniform buffer variables.
            //Parameters define the position in the buffer to write to 
            //(returned from addSize) and a memory address from which to read
            //the data to be copied.
            virtual void setValue(uint32 index, void* data) = 0;

            //Creates a new technique for the material. Parameters define the
            //camera ID, the quality setting and the current Level of Detail.
            //Returns the technique ID to be used to assign passes.
            virtual uint32 createTechnique(uint32 id, uint32 quality, 
               uint32 LOD) = 0;

            //Createa a new pass for the material. Parameter takes a material
            //pass description structure which it will use to create the 
            //pass. It will return a pass ID needed to associate a pass with 
            //a technique
            virtual uint32 createPass(const MaterialPassDescription&) = 0;

            //Adds a pass to a technique. Passes added to a technique will be
            //executed in the order in which they are added. Parameters define
            //the tecnique ID and the pass ID to be associated.
            virtual void addPassToTechnique(uint32 tech, uint32 pass) = 0;

            //Cleanly destroys the material, removing references from 
            //managers and deallocating resources.
            virtual void destroy() = 0;
         };
      }
   }

#endif