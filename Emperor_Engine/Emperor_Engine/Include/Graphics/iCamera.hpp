#ifndef __EMP_GFX_I_CAMERA_HPP__
#define __EMP_GFX_I_CAMERA_HPP__

//Basic Graphic Types
#include "Types.hpp"

//Core Scene Object required
#include "../Core/iSceneObject.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      class iTexture;

      /** Camera Interface

      A single camera in the scene, a point in which to render from. The camera
      dictates an number of attributes that affect the way the scene will be
      rendered. All active cameras will cause the scene to be re-rendered from
      their perspectives and have their results written to their assoicated 
      render targets. The default render target is the back buffer (screen).
      */
      class iCamera : public iSceneObject
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iCamera() {}
            virtual ~iCamera() {}
         public:
            //Sets the camera's vertical field of view, the angle of viewable
            //space for the camera
            virtual void setFOV(float) = 0;

            //Returns the camera's vertical field of view
            virtual float getFOV() const = 0;

            //Alters the order in which cameras are rendered from. The higher
            //the priority, the earlier they will be rendered in the camera
            //queue
            virtual void setPriority(float) = 0;

            //Returns the camera's priority
            virtual float getPriority() const = 0;

            //Sets the camera's horizonal field of view as a ratio of vertical
            //field of view
            virtual void setAspect(float) = 0;

            //Returns the camera's horizontal field of view ratio
            virtual float getAspect() const = 0;

            //Sets the camera's near clipping plane. Anything closer to than 
            //the clipping plane will clipped (not visible)
            virtual void setNearClipping(float) = 0;

            //Returns the camera's near clipping plane
            virtual float getNearClipping() const = 0;

            //Sets the camera's far clipping plane. Anything farther to than 
            //the clipping plane will clipped (not visible)
            virtual void setFarClipping(float) = 0;

            //Returns the camera's far clipping plane
            virtual float getFarClipping() const = 0;

            //Sets the camera's color render target. When rendered from, the 
            //output will be written to the target instead of the backbuffer
            virtual void setRenderTarget(const iTexture*) = 0;

            //Sets the camera's depth render target. When rendered from , the
            //depth values will be written to the target instead of the 
            //the system's depth target
            virtual void setDepthStencil(const iTexture*) = 0;

            //Sets whether to use the system's backbuffer/depth buffer when not
            //provided or to not use one at all
            virtual void useExclusiveTarget(bool) = 0;

            //Describes how the raster operations should be performed when 
            //rendering from the camera
            virtual void setRasterState(const RasterizerStateDescription&) = 0;

            //Sets the viewport of the camera. The viewport indicates what 
            //portion of the render target the camera's render will take up
            virtual void setViewPort(uint32 left, uint32 top, uint32 width, 
               uint32 height) = 0;
            virtual void setViewPort(const Vector<uint32, 4>&) = 0;

            //Returns the camera's view port
            virtual Vector<uint32, 4> getViewPort() const = 0;
         };
      }
   }

#endif