#ifndef __EMP_GFX_I_VERTEX_FORMAT_HPP__
#define __EMP_GFX_I_VERTEX_FORMAT_HPP__

//Basic Graphic Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      class iVertexFormat
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iVertexFormat() {}
            virtual ~iVertexFormat() {}
         public:
            //Adds a type to the type list of the vertex format. The parameter
            //determines the binding semantic of the vfalue (DX only). Once
            //added, it cannot be removed. All values are added in the order
            //they are provided
            virtual void addFloat(const LString&) = 0;
            virtual void addInt(const LString&) = 0;
            virtual void addUint(const LString&) = 0;
            virtual void addVector2(const LString&) = 0;
            virtual void addVector3(const LString&) = 0;
            virtual void addVector4(const LString&) = 0;

            //Returns the number of bytes the vertex format is currently
            virtual uint32 getByteSize() const = 0;

            //Returns the number of elements that make up the vertex currently
            virtual uint32 getNumOfElements() const = 0;

            //Returns the vertex description in the form of an array of
            //input elements
            virtual LArray<InputElement> getVertexDescription() const = 0;

            //Returns the byte indicies of the elements in the format as an
            //array
            virtual LArray<uint32> getDynamicIndex() const = 0;

            //Cleanly destroys the object, removing references in managers and
            //deallocating resources.
            virtual void destroy() = 0;
         };
      }
   }
#endif
