#ifndef __EMP_GFX_I_BLEND_STATE_HPP__
#define __EMP_GFX_I_BLEND_STATE_HPP__

//Basic Graphic Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      /** Blend State Interface

      A resource object that dictates how blending should be performed during
      rendering. Blend states are a required component of the material system
      and need to be bound before each rendering pass.
      */
      class iBlendState
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iBlendState() {}
            virtual ~iBlendState() {}
         public:
            //Safely destroys the resource, removing it from the blend state
            //resource manager
            virtual void destroy() = 0;

            //Performs device level preparations with the description set from
            //the setDescription function, will fail if description isn't set
            virtual void finalize() = 0;

            //Defines the blend state's operation for a particular render 
            //target using the provided description. If independent blend is
            //turned off, this sets the blend state operation for all targets
            virtual void setDescription(const BlendStateDescription&, 
               uint32 renderTarget) = 0;

            //Indicates if the alpha should be interpreted as coverage data for
            //multisampling
            virtual void setAlphaToCoverage(bool) = 0;

            //Indicates if each render target will have a different blend state
            //or if all will use the same blend state
            virtual void setIndependantBlend(bool) = 0;
         };
      }
   }

#endif