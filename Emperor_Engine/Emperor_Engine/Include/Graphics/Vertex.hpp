#ifndef __EMP_GFX_VERTEX_HPP__
#define __EMP_GFX_VERTEX_HPP__

#include <cstring>
#include "iVertexFormat.hpp"
#include "Types.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      class Vertex
         {
         private:
         protected:
            const iVertexFormat* format;
            byte* data;
         public:
            Vertex(const iVertexFormat& vertexFormat) : format(&vertexFormat)
               {
               data = new byte[format->getByteSize()];
               }

            Vertex(const Vertex& v) : format(nullptr), data(nullptr)
               {
               *this = v;
               }

            virtual ~Vertex()
               {
               if(data)
                  delete[] data;
               }

            void fill(byte* b, uint32 s)
               {
               if(data)
                  {
                  memcpy(data, b, s);
                  }
               }

            void fill(byte* b)
               {
               if(data)
                  {
                  fill(b, format->getByteSize());
                  }
               }

            template <class T>
            void setData(const T& d, uint32 index)
               {
               if(data)
                  {
                  auto& di = format->getDynamicIndex();
                  if(index < di.size())
                     memcpy(data + di[index], &d, sizeof(d));
                  }
               }

            const byte* getData() const { return data; }
            void getData(byte* d) const
               {
               if(data)
                  memcpy(d, data, format->getByteSize());
               }

            const iVertexFormat& getVertexFormat() const
               { return *format; }

            Vertex& Vertex::operator=(const Vertex& v)
               {
               if(this != &v)
                  {
                  if(data)
                     delete[] data;
                  data = 0;

                  if(v.data)
                     {
                     uint32 sz = v.format->getByteSize();
                     data = new byte[sz];
                     memcpy(data, v.data, sz);
                     }
                  format = v.format;
                  }

               return *this;
               }
         };
      }
   }


#endif