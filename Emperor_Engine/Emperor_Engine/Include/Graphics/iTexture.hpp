#ifndef __EMP_GFX_I_TEXTURE_HPP__
#define __EMP_GFX_I_TEXTURE_HPP__

//Basic Graphic Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      /** Texture Interface

      A general purpose texture resource. A texture is a collection of color 
      pixels grouped into an image of some kind. Textures can be a 1D, 2D, 3D 
      or cube texture (not currently supported).
      */
      class iTexture
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iTexture() {}
            virtual ~iTexture() {}
         public:
            //Loads a texture from file. Currently supported formats:
            //PNG, JPEG, BMP, DDS (directX only)
            virtual void loadFile(const LString& fileName) = 0;

            //Cleanly destroys the object, removing references in managers and
            //deallocating resources.
            virtual void destroy() = 0;
         };

      /** 1D Texture Interface

      A specialized texture templates to be used with 1D custom textures. Gives
      the ability to initialize with specific size and pixel format.
      */
      class iTexture1D
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iTexture1D() {}
            virtual ~iTexture1D() {}
         public:
            //Initializes the texture on the graphics card. Parameters define
            //the number of pixels in the texture and the format of the pixels.
            virtual void initCustom(uint32 size, StructureFormat format) = 0;

            //Fills an already initialized texture. Parameters provide the data
            //to be copied into the texture and the number of bytes to copy.
            virtual void fill(byte* data, uint32 size) = 0;
         };

      /** 2D Texture Interface

      A specialized texture templates to be used with 2D custom textures. Gives
      the ability to initialize with specific size and pixel format.
      */
      class iTexture2D
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iTexture2D() {}
            virtual ~iTexture2D() {}
         public:
            //Initializes the texture on the graphics card. Parameters define
            //the number of pixels (height and width) in the texture and the 
            //format of the pixels.
            virtual void initCustom(uint32 width, uint32 height, 
               StructureFormat format) = 0;

            //Fills an already initialized texture. Parameters provide the data
            //to be copied into the texture and the number of bytes to copy.
            virtual void fill(byte* data, uint32 size) = 0;
         };

      /** 3D Texture Interface

      A specialized texture templates to be used with 3D custom textures. Gives
      the ability to initialize with specific size and pixel format.
      */
      class iTexture3D
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iTexture3D() {}
            virtual ~iTexture3D() {}
         public:
            //Initializes the texture on the graphics card. Parameters define
            //the number of pixels (height, width and depth) in the texture
            //and the format of the pixels.
            virtual void initCustom(uint32 width, uint32 height, uint32 depth,
               StructureFormat format) = 0;

            //Fills an already initialized texture. Parameters provide the data
            //to be copied into the texture and the number of bytes to copy.
            virtual void fill(byte* data, uint32 size) = 0;
         };
      }
   }

#endif