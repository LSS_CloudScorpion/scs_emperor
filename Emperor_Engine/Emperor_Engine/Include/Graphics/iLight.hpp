#ifndef __EMP_GFX_I_LIGHT_HPP__
#define __EMP_GFX_I_LIGHT_HPP__

//Basic Graphic Types
#include "Types.hpp"

//Core Scene Object required
#include "../Core/iSceneObject.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      /** Light Interface

      A single dynamic light in the scene. Lights are used to illuminate the 
      scene and are important for basic lighting effects. Each additional light
      may cause an object to be rendered an additional time (depending on 
      material and shader implementation).
      */
      class iLight : public iSceneObject
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iLight() {}
            virtual ~iLight() {}
         public:
            //Sets what type of light the light will act as. Directional lights
            //cast light in a single direction without an origin. Point lights
            //cast light in all directions from a point of origin. Spotlights
            //cast light in a set cone from a point of origin.
            virtual void setLightType(LightType) = 0;

            //Returns the type of light the light is
            virtual LightType getLightType() const = 0;

            //Sets the light's diffuse color, indicating what color of light
            //will reflect off matte surfaces
            virtual void setDiffuse(const Color&) = 0;

            //Returns the light's diffuse color
            virtual Color getDiffuse() const = 0;

            //Sets the light's specular color, indicating what color of light
            //will reflect off glossy surfaces
            virtual void setSpecular(const Color&) = 0;

            //Returns the light's specular color
            virtual Color getSpecular() const = 0;

            //Set's the rate at which the light's intensity diminishes over 
            //distance. Parameters determine the intensity of the light at
            //the point of origin (constant), the rate at which intensity is
            //lost per unit of distance (linear), and the rate at which 
            //intensity is lost per unit of distance squared (quadratic).
            //Does not effect directional lights.
            virtual void setAttenuation(float constant, float linear, float
               quadratic) = 0;
            virtual void setAttenuation(const Vector<float, 3>&) = 0;

            //Returns the rate of which the light's intensity dimishes over
            //distance.
            virtual Vector<float, 3> getAttenuation() const = 0;

            //Sets the cone of the spotlight. The parameters indicate the
            //angle of effect for the spotlight, which consists of the angle 
            //at which any light will be emitted (umbra), the angle at which
            //full intensity light will be emitted (penumbra), and the rate
            //at which the light will dimished between the umbra and penumbra
            //(falloff). This only effects spotlights.
            virtual void setSpot(float umbra, float penumbra, float falloff)
               = 0;
            virtual void setSpot(const Vector<float, 3>&) = 0;

            //Returns the cone of the spotlight.
            virtual Vector<float, 3> getSpot() const = 0;

            //Sets the maximum range of the light. Any actors outside of this
            //range will not be affected by the light.
            virtual void setRange(float) = 0;

            //Retursn the maximum range of the light.
            virtual float getRange() = 0;

            //Creats and configures the shadow texture to be used for the 
            //light. Paramters detail the size of texture to be used in pixels.
            virtual void configureShadowTexture(uint32 width, uint32 height)=0;

            //Sets whether or not the light will cast shadows. Turning this
            //on causes another render of the scene and can be costly. Should
            //only be activated on spotlights.
            virtual void enableShadows(bool) = 0;

            //Returns true if the light casts shadows, false otherwise.
            virtual bool castsShadows() = 0;
         };
      }
   }

#endif