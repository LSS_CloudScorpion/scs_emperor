#ifndef __EMP_GFX_I_TEXTURE_SAMPLER_HPP__
#define __EMP_GFX_I_TEXTURE_SAMPLER_HPP__

//Basic Graphic Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      /** Texture Sampler Interface

      A texture sampling resource. Texture samplers contain instruction on how
      to sample from a texture from within the graphics card which will affect
      the speed and quality of the sampling methods that texture are sampled 
      by.
      */
      class iTextureSampler
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iTextureSampler() {}
            virtual ~iTextureSampler() {}
         public:
            //Builds the texture sampler on the device with the parameters
            //provided
            virtual void build() = 0;

            //Cleanly destroys the material, removing references from 
            //controllers and deallocating resources.
            virtual void destroy() = 0;

            //Returns the texture filter method
            virtual const TextureFilter& getFilter() const = 0;

            //Sets the texture filter of the sampler. This dictates how the 
            //a single texel will be sampled from the texture
            virtual void setFilter(const TextureFilter& filter) = 0;

            //Returns the address mode on the U axis
            virtual TextureAddressMode getAddressModeU() const = 0;

            //Sets the address mode on the U axis. The address mode dictates 
            //where the texel will be sampled from when the U coord is outside
            //the range of 0 and 1
            virtual void setAddressModeU(TextureAddressMode) = 0;

            //Returns the address mode on the V axis
            virtual TextureAddressMode getAddressModeV() const = 0;

            //Sets the address mode on the V axis. The address mode dictates 
            //where the texel will be sampled from when the V coord is outside
            //the range of 0 and 1
            virtual void setAddressModeV(TextureAddressMode) = 0;

            //Returns the address mode on the W axis
            virtual TextureAddressMode getAddressModeW() const = 0;

            //Sets the address mode on the W axis. The address mode dictates 
            //where the texel will be sampled from when the W coord is outside
            //the range of 0 and 1
            virtual void setAddressModeW(TextureAddressMode) = 0;

            //Returns the level of detail bias for the mip-map
            virtual float getMipLODBias() const = 0;

            //Sets the level of detail bias for the mip-map. This controls how
            //the sampler will favor the highest mip level over lower ones as
            //the object gets farther away from the screen
            virtual void setMipLODBias(float) = 0;

            //Returns the maximum anisotropy level for the sampler
            virtual uint32 getMaxAnisotropy() const = 0;

            //Sets the maximum anisotropy level for the sampler. This limits
            //the level of anisotropic filtering that can be sampled from 
            //a texture
            virtual void setMaxAnisotropy(uint32) = 0;

            //Returns the comparison method for the sampler
            virtual TextureComparisonMethod getComparisonMethod() const = 0;

            //Sets the comparison method for the sampler. The comparison method
            //is useful for comparing values on the hardware when the actual 
            //value of the texture is sampled simply for comarison reasons,
            //such as sampling depth textures for shadows
            virtual void setComparisonMethod(TextureComparisonMethod) = 0;

            //Returns the border color for the sampler
            virtual const Color& getBorderColor() const = 0;

            //Sets the border color of the sampler. The border color will be 
            //sampled when the U, V, or W coord is outside of the range of 
            //0 and 1 and the address mode is set to border
            virtual void setBorderColor(const Color&) = 0;

            //Returns the minimum range of detail for the mip-map
            virtual float getMinLOD() const = 0;

            //Sets the minimum range of detail for the mip-map. This indicates
            //the highest quality of the mip-map, clamped to zero
            virtual void setMinLOD(float) = 0;

            //Returns the maximum range of detail for the mip-map
            virtual float getMaxLOD() const = 0;

            //Sets the maximum range of detail for the mip-map. This indicates
            //the lowest quality of the mip-map, can be any number up to the
            //maximum float value. Must be above the minimum LOD.
            virtual void setMaxLOD(float) = 0;
         };
      }

   }

#endif