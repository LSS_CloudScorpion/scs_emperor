#ifndef __EMP_GFX_I_RESOURCE_CONTROLLER_HPP__
#define __EMP_GFX_I_RESOURCE_CONTROLLER_HPP__

namespace Emperor
   {
   namespace Graphics
      {
      //Prototypes for other interfaces
      //Reduces need for includes
      class iVertexFormat;
      class iMesh;
      class iMaterial;
      class iTexture;
      class iTextureSampler;
      class iBlendState;
      class iTexture1D;
      class iTexture2D;
      class iTexture3D;

      /** Resource Controller Interface

      A simple controller class meant to provide the client programmer with 
      the ability to retreive resources from the resource managers and create
      custom resources.
      */
      class iResourceController
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iResourceController() {}
            virtual ~iResourceController() {}
         public:
            //Retreive a resource from the resource controllers. If the 
            //resource is not already loaded, the controller will attempt to
            //load the resource. Accepts a resource name or file name.
            virtual const iVertexFormat* getVertexFormat(const LString& name) = 0;
            virtual const iMesh* getMesh(const LString& name) = 0;
            virtual const iMaterial* getMaterial(const LString& name) = 0;
            virtual const iTexture* getTexture(const LString& name) = 0;
            virtual const iTextureSampler* getSampler(const LString& name) = 0;
            virtual const iBlendState* getBlendState(const LString& name) = 0;

            //Creates a new empty resource and registers it with the
            //associated resource manager. Accepts a resource name so it can 
            //be referenced for later use.
            virtual iVertexFormat* newVertexFormat(const LString& name) = 0;
            virtual iMesh* newCustomMesh(const LString& name) = 0;
            virtual iMaterial* newCustomMaterial(const LString& name) = 0;
            virtual iTexture1D* newCustomTexture1D(const LString& name) = 0;
            virtual iTexture2D* newCustomTexture2D(const LString& name) = 0;
            virtual iTexture3D* newCustomTexture3D(const LString& name) = 0;
            virtual iTexture2D* newCustomRenderTarget(const LString& name) = 0;
            virtual iTexture2D* newCustomDepthStencil(const LString& name) = 0;
            virtual iTextureSampler* newCustomSampler(const LString& name) = 0;
            virtual iBlendState* newBlendState(const LString& name) = 0;
         };
      }
   }

#endif